# Sisop Praktikum Modul 3 2023 AM B11
Anggota Kelompok ``B11`` 
| Nama                      | NRP        |
|---------------------------|------------|
| Rayhan Arvianta Bayuputra | 5025211217 |
| Danno Denis Dhaifullah    | 5025211027 |
| Samuel Berkat Hulu*        | 5025201055 |

ps: *tidak ikut mengerjakan Praktikum


## Nomor 1
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 

a. Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

b. Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

c. Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.

d. Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 

e. Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

## JAWABAN

```c
#include<stdatomic.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>

typedef struct HuffmanNode {
	int frequency;
	char character;
	struct HuffmanNode *left, *right;
} HuffmanNode;

HuffmanNode *createHuffmanNode() {
	HuffmanNode *h = malloc(sizeof(*h));
	h->character = 0;
	h->frequency = 0;
	h->left = NULL;
	h->right = NULL;
	return h;
}

HuffmanNode *createParent(HuffmanNode *left, HuffmanNode *right) {
	HuffmanNode *h = malloc(sizeof(*h));
	h->character = 0;
	h->frequency = left->frequency + right->frequency;
	h->left = left;
	h->right = right;
	return h;
}

typedef struct PQueueNode {
	HuffmanNode *data;
	struct PQueueNode *next;
} PQueueNode;

typedef struct PQueue {
	PQueueNode *top;
	int size;
} PQueue;

PQueueNode *createPQNode(HuffmanNode *data) {
	PQueueNode *newNode = malloc(sizeof(*newNode));
	newNode->data = data;
	newNode->next = NULL;
	return newNode;
}

PQueue *createPQueue() {
	PQueue *pq = malloc(sizeof(*pq));
	pq->top = NULL;
	pq->size = 0;
	return pq;
}

int pqIsEmpty(PQueue *pq) {
	return (pq->top == NULL);
}

void pqPush(PQueue *pq, HuffmanNode *data) {
	PQueueNode *temp = pq->top;
	PQueueNode *newNode = createPQNode(data);
	pq->size++;

	if (pqIsEmpty(pq)) {
		pq->top = newNode;
		return;
	}

  	if(data->frequency < pq->top->data->frequency) {
    	newNode->next = pq->top;
    	pq->top = newNode;
  	} else {
    
	while(temp->next != NULL && temp->next->data->frequency < data->frequency) {
      	temp = temp->next;
    }

	newNode->next = temp->next;
	temp->next = newNode;
	}
}

void pqPop(PQueue *pq) {
  	if (!pqIsEmpty(pq)) {
    	PQueueNode *temp = pq->top;
    	pq->top = pq->top->next;
    	pq->size--;
    	free(temp);
  }
}

HuffmanNode *pqTop(PQueue *pq) {
  	if(!pqIsEmpty(pq)) {
    	return pq->top->data;
  	}
  	return NULL;
}

HuffmanNode *createHuffmanTree(int charFrequency[]) {
  	PQueue *pq = createPQueue();

  	for(int i = 0; i < 26; i++) {
    	if (charFrequency[i] > 0) {
      		HuffmanNode *h = createHuffmanNode();
      		h->character = (char)(i + 'A');
      		h->frequency = charFrequency[i];
      		pqPush(pq, h);
    	}
  	}

  	while (pq->size > 1) {
    	HuffmanNode *left = pq->top->data;
    	pqPop(pq);

	    HuffmanNode *right = pq->top->data;
	    pqPop(pq);
	
	    HuffmanNode *parent = createParent(left, right);
	    pqPush(pq, parent);
  	}

  	return pq->top->data;
}

void traverseHuffmanTree(HuffmanNode *root, char *code, char huffmanMap[][16]) {
  	if (root->left != NULL) {
		char new_code[128];
		strcpy(new_code, code);
		traverseHuffmanTree(root->left, strcat(new_code, "0"), huffmanMap);
	
	    strcpy(new_code, code);
	    traverseHuffmanTree(root->right, strcat(new_code, "1"), huffmanMap);
  	} else {
	    strcpy(huffmanMap[(int)(root->character - 'A')], code);
  	}
}


void cmd(char *command[]) {
  	pid_t pid = fork();

  	if(pid < 0){
	    printf("ERROR: fork() failed\n");
	    exit(EXIT_FAILURE);
  	} else if(pid == 0){
    	if(execvp(command[0], command) == -1){
      		exit(EXIT_FAILURE);
    	}
  	} else {
    	int status;
    	wait(&status);
  	}
}


int main() {
  	if(fopen("file.txt", "r") == NULL){
    	char *url = "https://drive.google.com/u/0/" "uc?id=1HWx1BGi1rEZjoEjzhAuKMAcdI27wQ41C&export=download";
    	cmd((char *[]){"wget", url, "-O", "file.txt", "-q", NULL});
  	}

  	int fd1[2], fd2[2];
  	if(pipe(fd1) < 0 || pipe(fd2) < 0){
    	fprintf(stderr, "ERROR: failed to create pipe!\n");
    	exit(1);
  	}

  	pid_t pid = fork();

  	if(pid < 0){
    	fprintf(stderr, "ERROR: failed to create fork\n");
    	exit(1);
  	}

  	if(pid == 0){
	    close(fd1[0]);
	    close(fd2[1]);
	
	    char text[1024];
	    int charFrequency[26];
	
		read(fd2[0], text, sizeof(text));
	    read(fd2[0], charFrequency, sizeof(charFrequency));
	
	    HuffmanNode *huffmanTree = createHuffmanTree(charFrequency);
	
	    char huffmanMap[26][16] = {""};
	    traverseHuffmanTree(huffmanTree, "", huffmanMap);
	
	    printf("\n----- HUFFMAN CODE TABLE -----\n");
	    
		for(int i = 0; i < 26; i++){
	    	if(strcmp(huffmanMap[i], "") != 0){
	        	printf("%c: %s\n", (char)(i + 'A'), huffmanMap[i]);
	      	}
	    }

	    char huffmanEncodedText[4096] = "";
	    for (int i = 0; text[i] != '\0'; i++){
	      	if (text[i] >= 'A' && text[i] <= 'Z'){
	        	strcat(huffmanEncodedText, huffmanMap[text[i] - 'A']);
	      	}
	    }
	
    	printf("\n----- ENCODED TEXT -----\n");
    	printf("%s\n", huffmanEncodedText);

		write(fd1[1], huffmanMap, sizeof(huffmanMap));
    	write(fd1[1], huffmanEncodedText, strlen(huffmanEncodedText) + 1);
  	} else {
    	close(fd1[1]);
    	close(fd2[0]);

    	FILE *fp = fopen("./file.txt", "r");

    	char text[1024];
	    int charFrequency[26] = {0}, charCount = 0;
	    int c, i = 0;

	    while ((c = fgetc(fp)) != EOF) {
		  	char ch = (char)c;
		    if (ch >= 'a' && ch <= 'z') {
		      	ch -= 32;
		    	charFrequency[ch - 'A']++;
		        charCount++;
		    } else if (ch >= 'A' && ch <= 'Z') {
		        charFrequency[ch - 'A']++;
		        charCount++;
		    }
		    text[i++] = ch;
	    }

	    text[i] = '\0';
	
	    printf("----- ORIGINAL TEXT -----\n");
	    printf("%s\n", text);
	
	    write(fd2[1], text, sizeof(text));
	    write(fd2[1], charFrequency, sizeof(charFrequency));
	
	    char huffmanEncodedText[4096];
	    char huffmanMap[26][16];

	    HuffmanNode *huffmanTree = createHuffmanTree(charFrequency);
	
	    read(fd1[0], huffmanMap, sizeof(huffmanMap));
	    read(fd1[0], huffmanEncodedText, sizeof(huffmanEncodedText));
	
	    printf("\n----- DECODED TEXT -----\n");
	
	    HuffmanNode *explorer = huffmanTree;
	    for (int i = 0; huffmanEncodedText[i] != '\0'; i++) {
	      	if(huffmanEncodedText[i] == '0' && explorer->left != NULL) {
	        	explorer = explorer->left;
	      	} else if (huffmanEncodedText[i] == '1' && explorer->right != NULL) {
	        	explorer = explorer->right;
	      	}
	
	      	if (explorer->left == NULL && explorer->right == NULL) {
	        	printf("%c", explorer->character);
	        	explorer = huffmanTree; // back to root
	    	}
	    }
	    printf("\n");
	
		printf("\n----- SUMMARY -----\n");
		printf("Before compression\n");
	
		int totalUncompressed = charCount * 8;
		printf("Total size: %d bits\n\n", totalUncompressed);
	
		printf("After compression\n");
		int msgSize = 0;
	    
		for (int i = 0; i < 26; i++) {
	    	msgSize += charFrequency[i] * strlen(huffmanMap[i]);
	    }
	    printf("Message: %d bits\n", msgSize);
	
	    int tableSize = 0;
	    for (int i = 0; i < 26; i++) {
	      	if (strcmp(huffmanMap[i], "") != 0) {
	        	tableSize += 8 * strlen(huffmanMap[i]);
	      	}
	    }
	
	    printf("Table: %d bits\n", tableSize);
	
	    int totalCompressed = msgSize + tableSize;
	    printf("Total size: %d bits\n\n", msgSize + tableSize);
	
	    double reduction =
	        100 * (1.0 - ((double)totalCompressed / (double)totalUncompressed));
	    	printf("Size reduced by %.2f%%\n", reduction);
		}

  	return 0;
}
```

## PENJELASAN

```c
typedef struct HuffmanNode {
	int frequency;
	char character;
	struct HuffmanNode *left, *right;
} HuffmanNode;
```

- **Pertama**, buat struct dengan isi variabel ``frequency`` untuk mempresentasikan freakuensi karakter, ``character`` untuk karakter, dan ``struct HuffmanNode *left, *right`` untuk menunjuk pada _child_ kiri atau kanan dari node.


```c
HuffmanNode *createHuffmanNode() {
	HuffmanNode *h = malloc(sizeof(*h));
	h->character = 0;
	h->frequency = 0;
	h->left = NULL;
	h->right = NULL;
	return h;
}
```

- **Kedua**, buat fungsi untuk membuat _Huffman Node_.

- **``HuffmanNode *h``** adalah sebuah pointer yang menunjuk pada node Huffman.

- **``malloc(sizeof(*h))``** bertujuan untuk mengalokasikan memori untuk menampung node Huffman.

- **``h->character = 0;``** dan **``h->frequency = 0;``** digunakan untuk menginisialisasi variabel ``character`` dan ``frequency``.

- **``h->left = NULL``** dan **``h->right = NULL``** digunakan untuk menginisialisasi pointer ``left`` dan ``right``.




```c
HuffmanNode *createParent(HuffmanNode *left, HuffmanNode *right) {
	HuffmanNode *h = malloc(sizeof(*h));
	h->character = 0;
	h->frequency = left->frequency + right->frequency;
	h->left = left;
	h->right = right;
	return h;
}
```

- **Ketiga**, deklarasikan suatu fungsi untuk membuat sebuah node baru yang merupakan _parent_ dari node ``left`` dan ``right`` dan me-return pointer ke node baru tersebut.

- **``HuffmanNode *h``** adalah sebuah pointer yang menunjuk pada node Huffman.

- **``malloc(sizeof(*h))``** bertujuan untuk mengalokasikan memori untuk menampung node Huffman.

- **``h->character = 0``** digunakan untuk menginisialisasi variabel ``character``.

- **``h->frequency = left->frequency + right->frequency``**  digunakan untuk menghitung frekuensi sebagai jumlah frekuensi dari anak kiri dan anak kanannya.

-**``h->left = left dan h->right = right``** digunakan untuk menetapkan pointer ke node anak kiri dan anak kanan.


```c
typedef struct PQueueNode {
	HuffmanNode *data;
	struct PQueueNode *next;
} PQueueNode;

typedef struct PQueue {
	PQueueNode *top;
	int size;
} PQueue;
```

- **Keempat**, terdapat 2 buah struct bernama ``PQueueNode`` dengan isi variabel ``HuffmanNode *data`` sebagai pointer ke node Huffman dan ``struct PQueueNode *next`` sebagai pointer ke node berikutnya, lalu struct bernama ``PQueue`` dengan isi variabel ``PQueueNode *top`` sebagau pointer ke node pertama dan variabel ``int size`` yang merepresentasikan ukuran antrian.




```c
PQueueNode *createPQNode(HuffmanNode *data) {
	PQueueNode *newNode = malloc(sizeof(*newNode));
	newNode->data = data;
	newNode->next = NULL;
	return newNode;
}
```

- **Kelima**, ``createPQNode()`` bertujuan untuk membuat node baru dalam _priority queue_ dan me-return pointer ke node tersebut.

- **``PQueueNode *newNode``** adalah sebuah pointer yang menunjuk pada _priority queue_.

- **``malloc(sizeof(*newNode))``** bertujuan untuk mengalokasikan memori.

- **``newNode->data = data``** digunakan untuk menetapkan pointer ke node Huffman pada variabel data.

- **``newNode->next = NULL``** digunakan untuk menetapkan nilai ``NULL`` pada pointer ``next``..





```c
PQueue *createPQueue() {
	PQueue *pq = malloc(sizeof(*pq));
	pq->top = NULL;
	pq->size = 0;
	return pq;
}
```

- **Keenam**, ``createPQueue()`` bertujuan untuk membuat sebuah _priority queue_ baru dan me-rerturn pointer.

- **``PQueue *pq``** adalah sebuah pointer yang menunjuk pada _priority queue_ yang baru saja dibuat.

- **``malloc(sizeof(*pq))``** digunakan untuk mengalokasikan memori..

- **``pq->top = NULL``** digunakan untuk menetapkan nilai NULL pada pointer ``top``.

- **``pq->size = 0``** digunakan untuk menetapkan nilai 0 pada variabel ``size``.




```c
int pqIsEmpty(PQueue *pq) {
	return (pq->top == NULL);
}
```

- **Ketujuh**, ``pqIsEmpty()`` bertujuan untuk memeriksa apakah _priority queue_ yang diberikan kosong atau tidak.


```c
void pqPush(PQueue *pq, HuffmanNode *data) {
	PQueueNode *temp = pq->top;
	PQueueNode *newNode = createPQNode(data);
	pq->size++;

	if (pqIsEmpty(pq)) {
		pq->top = newNode;
		return;
	}

  	if(data->frequency < pq->top->data->frequency) {
    	newNode->next = pq->top;
    	pq->top = newNode;
  	} else {
    
	while(temp->next != NULL && temp->next->data->frequency < data->frequency) {
      	temp = temp->next;
    }

	newNode->next = temp->next;
	temp->next = newNode;
	}
}
```

- **Kedelapan**, ``pqPush()`` bertujuan untuk menambahkan sebuah node dengan data HuffmanNode ke dalam _priority queue_.

- **``PQueueNode *temp``** dan **``PQueueNode *newNode``** untuk menyimpan pointer ke node-node yang terkait dengan penambahan node baru.

- **``temp``** diisi dengan **``pq->top``**, yaitu pointer ke node teratas.

- **``newNode``** diisi dengan hasil fungsi **``createPQNode(data)``**, yaitu pembuatan sebuah node baru dengan data ``HuffmanNode`` yang diberikan.

- **``pq->size++``** menambahkan ukuran _priority queue_.

- **``if(pqIsEmpty(pq)){...}``** mengecek Jika _priority queue_ kosong, maka newNode akan menjadi node teratas.

- **``if(data->frequency < pq->top->data->frequency)``** mengecek jika frekuensi dari data HuffmanNode kurang dari frekuensi pada node teratas dari _priority queue_,maka ``newNode`` akan menjadi node teratas pada _priority queue_.

- **``while(temp->next != NULL && temp->next->data->frequency < data->frequency){...}``** mencari posisi yang tepat untuk memasukkan newNode.

- **``newNode->next = temp->next dan temp->next = newNode``** menambahkan newNode ke dalam _priority queue_ pada posisi yang tepat.




```c
void pqPop(PQueue *pq) {
  	if (!pqIsEmpty(pq)) {
    	PQueueNode *temp = pq->top;
    	pq->top = pq->top->next;
    	pq->size--;
    	free(temp);
  }
}
```

- **Kesembilan**, ``pqPop`` adalah untuk menghapus elemen teratas dari _priority queue_.


```c
HuffmanNode *pqTop(PQueue *pq) {
  	if(!pqIsEmpty(pq)) {
    	return pq->top->data;
  	}
  	return NULL;
}
```

- **Kesepuluh**, ``pqTop`` bertujuan untuk mengembalikan data dari elemen teratas pada _priority queue_, yaitu HuffmanNode *.


```c
HuffmanNode *createHuffmanTree(int charFrequency[]) {
  	PQueue *pq = createPQueue();

  	for(int i = 0; i < 26; i++) {
    	if (charFrequency[i] > 0) {
      		HuffmanNode *h = createHuffmanNode();
      		h->character = (char)(i + 'A');
      		h->frequency = charFrequency[i];
      		pqPush(pq, h);
    	}
  	}

  	while (pq->size > 1) {
    	HuffmanNode *left = pq->top->data;
    	pqPop(pq);

	    HuffmanNode *right = pq->top->data;
	    pqPop(pq);
	
	    HuffmanNode *parent = createParent(left, right);
	    pqPush(pq, parent);
  	}

  	return pq->top->data;
}
```

- **Kesebelas**, ``createHuffmanTree()`` digunakan untuk membuat _Huffman Tree_ berdasarkan frekuensi karakter yang diberikan. 

- **``PQueue *pq = createPQueue();``** membuat _priority queue_.

- **``for(int i=0; i<26; i++){...}``** memeriksa jika frekuensi karakter lebih besar dari 0, maka dibuatkan node baru dengan karakter dan frekuensi tersebut dan dimasukkan ke dalam _priority queue_ dengan memanggil fungsi ``pqPush()``.

- **``while(pq->size > 1){...}``** bertujuan bahwa selama ukuran _priority queue_ lebih besar dari 1, ambil 2 node dengan frekuensi terkecil dari _priority queue_ menggunakan ``pqTop()`` dan ``pqPop()``. Kemudian, buat _parent node_ baru menggunakan fungsi ``createParent()`` dengan node kiri dan kanan tersebut dan masukkan ke dalam _priority queue_ menggunakan ``pqPush()``.





```c
void traverseHuffmanTree(HuffmanNode *root, char *code, char huffmanMap[][16]) {
  	if (root->left != NULL) {
		char new_code[128];
		strcpy(new_code, code);
		traverseHuffmanTree(root->left, strcat(new_code, "0"), huffmanMap);
	
	    strcpy(new_code, code);
	    traverseHuffmanTree(root->right, strcat(new_code, "1"), huffmanMap);
  	} else {
	    strcpy(huffmanMap[(int)(root->character - 'A')], code);
  	}
}
```

- Fungsi ``traverseHuffmanTree()`` digunakan untuk mengelilingi _Huffman Tree_ dan menyusun map kode Huffman untuk setiap karakter pada tree.

- Fungsi menerima 3 parameter: ``root`` yang merupakan pointer ke root node yang ingin di-_traverse_; ``code`` yang merupakan string untuk node saat ini; ``huffmanMap`` yang merupakan array untuk menyimpan map untuk setiap karakter pada tree.

- Pada setiap pemanggilan fungsi, cek apakah node saat ini adalah _leaf node_ atau bukan. Jika bukan _leaf node_, maka rekursi ke _child node_ yang ada dan tambahkan 0 ke ``code`` untuk _left child_, dan tambahkan 1 untuk right child.

- Jika node saat ini adalah _leaf node_, copy string ``code`` ke ``huffmanMap`` pada indeks yang sesuai dengan karakter yang diwakili oleh leaf node tersebut.




```c
void cmd(char *command[]) {
  	pid_t pid = fork();

  	if(pid < 0){
	    printf("ERROR: fork() failed\n");
	    exit(EXIT_FAILURE);
  	} else if(pid == 0){
    	if(execvp(command[0], command) == -1){
      		exit(EXIT_FAILURE);
    	}
  	} else {
    	int status;
    	wait(&status);
  	}
}
```

- Fungsi ``cmd`` menjalankan _command_ shell di dalam sebuah _child process_.

- Fungsi ``fork()`` digunakan untuk membuat _child process_ baru.

- Jika nilai ``fork()`` kurang dari 0, menandakan program error.

- Jika nilai ``fork()`` sama dengan 0, berarti program sedang berada dalam _child process_. Di dalam _child process_, fungsi ``execvp()`` menjalankan _command_ yang diberikan.

- Jika ``execvp()`` bernilai -1, menandakan terjadi kesalahan dan _child process_ akan keluar.

- Jika ``fork()`` bernilai selain 0, berarti program sedang berada dalam _parent process_. Di dalam _parent process_, fungsi ``wait()`` akan dipanggil untuk menunggu _child process_ selesai dijalankan.




```c
int main(){
  	if(fopen("file.txt", "r") == NULL){
    	char *url = "https://drive.google.com/u/0/" "uc?id=1HWx1BGi1rEZjoEjzhAuKMAcdI27wQ41C&export=download";
    	cmd((char *[]){"wget", url, "-O", "file.txt", "-q", NULL});
  	}

  	int fd1[2], fd2[2];
  	if(pipe(fd1) < 0 || pipe(fd2) < 0){
    	fprintf(stderr, "ERROR: failed to create pipe!\n");
    	exit(1);
  	}

  	pid_t pid = fork();

  	if(pid < 0){
    	fprintf(stderr, "ERROR: failed to create fork\n");
    	exit(1);
  	}

  	if(pid == 0){
	    close(fd1[0]);
	    close(fd2[1]);
	
	    char text[1024];
	    int charFrequency[26];
	
		read(fd2[0], text, sizeof(text));
	    read(fd2[0], charFrequency, sizeof(charFrequency));
	
	    HuffmanNode *huffmanTree = createHuffmanTree(charFrequency);
	
	    char huffmanMap[26][16] = {""};
	    traverseHuffmanTree(huffmanTree, "", huffmanMap);
	
	    printf("\n----- HUFFMAN CODE TABLE -----\n");
	    
		for(int i = 0; i < 26; i++){
	    	if(strcmp(huffmanMap[i], "") != 0){
	        	printf("%c: %s\n", (char)(i + 'A'), huffmanMap[i]);
	      	}
	    }

	    char huffmanEncodedText[4096] = "";
	    for (int i = 0; text[i] != '\0'; i++){
	      	if (text[i] >= 'A' && text[i] <= 'Z'){
	        	strcat(huffmanEncodedText, huffmanMap[text[i] - 'A']);
	      	}
	    }
	
    	printf("\n----- ENCODED TEXT -----\n");
    	printf("%s\n", huffmanEncodedText);

		write(fd1[1], huffmanMap, sizeof(huffmanMap));
    	write(fd1[1], huffmanEncodedText, strlen(huffmanEncodedText) + 1);
  	} else {
    	close(fd1[1]);
    	close(fd2[0]);

    	FILE *fp = fopen("./file.txt", "r");

    	char text[1024];
	    int charFrequency[26] = {0}, charCount = 0;
	    int c, i = 0;

	    while ((c = fgetc(fp)) != EOF) {
		  	char ch = (char)c;
		    if (ch >= 'a' && ch <= 'z') {
		      	ch -= 32;
		    	charFrequency[ch - 'A']++;
		        charCount++;
		    } else if (ch >= 'A' && ch <= 'Z') {
		        charFrequency[ch - 'A']++;
		        charCount++;
		    }
		    text[i++] = ch;
	    }

	    text[i] = '\0';
	
	    printf("----- ORIGINAL TEXT -----\n");
	    printf("%s\n", text);
	
	    write(fd2[1], text, sizeof(text));
	    write(fd2[1], charFrequency, sizeof(charFrequency));
	
	    char huffmanEncodedText[4096];
	    char huffmanMap[26][16];

	    HuffmanNode *huffmanTree = createHuffmanTree(charFrequency);
	
	    read(fd1[0], huffmanMap, sizeof(huffmanMap));
	    read(fd1[0], huffmanEncodedText, sizeof(huffmanEncodedText));
	
	    printf("\n----- DECODED TEXT -----\n");
	
	    HuffmanNode *explorer = huffmanTree;
	    for (int i = 0; huffmanEncodedText[i] != '\0'; i++) {
	      	if(huffmanEncodedText[i] == '0' && explorer->left != NULL) {
	        	explorer = explorer->left;
	      	} else if (huffmanEncodedText[i] == '1' && explorer->right != NULL) {
	        	explorer = explorer->right;
	      	}
	
	      	if (explorer->left == NULL && explorer->right == NULL) {
	        	printf("%c", explorer->character);
	        	explorer = huffmanTree; // back to root
	    	}
	    }
	    printf("\n");
	
		printf("\n----- SUMMARY -----\n");
		printf("Before compression\n");
	
		int totalUncompressed = charCount * 8;
		printf("Total size: %d bits\n\n", totalUncompressed);
	
		printf("After compression\n");
		int msgSize = 0;
	    
		for (int i = 0; i < 26; i++) {
	    	msgSize += charFrequency[i] * strlen(huffmanMap[i]);
	    }
	    printf("Message: %d bits\n", msgSize);
	
	    int tableSize = 0;
	    for (int i = 0; i < 26; i++) {
	      	if (strcmp(huffmanMap[i], "") != 0) {
	        	tableSize += 8 * strlen(huffmanMap[i]);
	      	}
	    }
	
	    printf("Table: %d bits\n", tableSize);
	
	    int totalCompressed = msgSize + tableSize;
	    printf("Total size: %d bits\n\n", msgSize + tableSize);
	
	    double reduction =
	        100 * (1.0 - ((double)totalCompressed / (double)totalUncompressed));
	    	printf("Size reduced by %.2f%%\n", reduction);
		}

  	return 0;
}
```

- Program tersebut melakukan kompresi pada sebuah teks yang ada pada file ``file.txt``. Program akan melakukan kompresi teks dengan membangun _Huffman Tree_ dan meletakkan setiap karakter ke kode Huffman-nya. Setelah itu, program akan mengganti setiap karakter dalam teks dengan kode Huffman-nya. Selain itu, program juga akan menampilkan informasi tentang teks yang telah dikompresi, seperti tabel kode Huffman, teks asli, teks yang telah dikompresi, dan ukuran teks sebelum dan sesudah dikompresi. Program juga akan menampilkan ukuran total yang diperlukan untuk menyimpan tabel dan teks yang telah dikompresi.



## Nomor 2
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.

a. Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

b. Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)

c. Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.

d. Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

### Soal A
**Solusi :**
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

int main() {
    
    key_t key = 2104;
    int (*result)[4][5];

    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
    result = shmat(shmid, NULL, 0);

    int matrix1[4][2];
    int matrix2[2][5];
    int i, j, k;

    // Generate random numbers for matrix 1
    srand(time(NULL));
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 2; j++) {
            matrix1[i][j] = rand() % 5 + 1;
        }
    }

    // Generate random numbers for matrix 2
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 5; j++) {
            matrix2[i][j] = rand() % 4 + 1;
        }
    }

    // Initialize result matrix with 0
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            (*result)[i][j] = 0;
        }
    }

    // Multiply matrix 1 and matrix 2
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            for (k = 0; k < 2; k++) {
                (*result)[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }

    // Display result matrix
    printf("Result matrix:\n");
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("[%d] ", (*result)[i][j]);
        }
        printf("\n");
    }

    return 0;
}
```
**Penjelasan Kode :**
- Didefinisikan nilai key untuk mengidentifikasi segmen shared memory yang bernilai ``2104``.
- Kemudian, mendeklarasi variable ``result`` yang akan menyimpan hasil dari perkalian matrix yang akan disimpan pada alamat memory yang shared.
- Menginisiasi segment shared memory menggunakan fungsi ``shmget``. Fungsi ini mengambil kunci, ukuran segmen memori (yang merupakan ukuran array 2D dengan ukuran 4x5), dan flag izin ``(IPC_CREAT | 0666)`` sebagai argumen.
- Segment shared memory yang diidentifikasi oleh ``shmid`` dilampirkan ke proses saat ini menggunakan fungsi ``shmat``.
- Melakukan perkalian 2 matrix, dimana matrix pertama diisi dengan angka random dari 1-5 (inklusif) dan matrix kedua diisi dengan angka random dari 1-4 (inklusif). Pengisian random tersebut dilakukan dengan menggunakan fungsi ``rand()``. Hasil perkaliannya akan disimpan ke array 2D matrix ``result`` yang awalnya diinisiasi dengan mengisikan nilai 0 pada seluruh baris dan kolomnya. Setelah melakukan perkalian dan penyimpanan ke matrix ``result``, matrix tersebut diprint.

### Soal B 
**Solusi :**
```c
int (*result)[4][5];
    ...
    key_t key = 2104;
    int shmid = shmget(key, sizeof(int[4][5]), 0666);
    result = shmat(shmid, NULL, 0);

    int i, j;
    printf("\nResult matrix:\n");
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            printf("[%d] ", (*result)[i][j]);
        }
        printf("\n");
    }
    ...
```
**Penjelasan Kode :**
- Dengan menggunakan shared memory, matrix result akan diakses menggunakan fungsi ``shmget`` yang mengembalikan ID shared memory berdasarkan ``key`` yang telah ditentukan. Key pada program ``cinta.c`` harus sama dengan ``kalian.c``. Dengan ini, alamat matrix result dapat diakses oleh ``(*result)[4][5]``.  
- Kemudian, matrix di print seperti biasa.

### Soal C 
**Solusi :**
```c
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

#define ull unsigned long long

typedef struct
{
    int i;
    int j;
} indexing;

int (*result)[4][5];
ull result_factorial[4][5];
pthread_t t_id[20];

void *calculate_factorial(void *arg)
{
    indexing *indx = (indexing *)arg;
    int i = indx->i;
    int j = indx->j;
    ull factorial = 1;
    for (int k = 1; k <= (*result)[i][j]; k++)
    {
        factorial *= k;
    }
    result_factorial[i][j] = factorial;
}

int main()
{
    clock_t start, end;
    double cpu_time_used;

    start = clock();

    key_t key = 2104;
    int shmid = shmget(key, sizeof(int[4][5]), 0666);
    result = shmat(shmid, NULL, 0);

    int i, j;
    printf("\nResult matrix:\n");
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            printf("[%d] ", (*result)[i][j]);
        }
        printf("\n");
    }

    printf("\nHasil faktorial matrix:\n");
    indexing *indx[20];
    int row, col;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            indx[i] = malloc(sizeof(indx));
            indx[i]->i = i;
            indx[i]->j = j;
            pthread_create(&t_id[i * 5 + j], NULL, &calculate_factorial, (void *)indx[i]);
        }
    }

    for (i = 0; i < 20; i++)
    {
        pthread_join(t_id[i], NULL);
    }

    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            printf("[%llu] ", result_factorial[i][j]);
        }
        printf("\n");
    }

    end = clock();
    cpu_time_used = ((double)(end - start)) * 1000 / CLOCKS_PER_SEC;
    printf("\nCPU elapsed time when running: %f ms\n", cpu_time_used);
    return 0;
}
```
**Penjelasan Kode :**
- Setelah mendapatkan matrix result menggunakan konsep shared memory, dilakukan perhitungan nilai faktorial dari setiap elemen pada matrix tersebut. Hal ini dilakukan dengan konsep multithreading, dimana setiap thread melakukan perhitungan faktorial untuk setiap kolom dengan memanggil fungsi ``void *calculate_factorial(void *arg)``.

```c
    ...
    indexing *indx[20];
    int row, col;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            indx[i] = malloc(sizeof(indx));
            indx[i]->i = i;
            indx[i]->j = j;
            pthread_create(&t_id[i * 5 + j], NULL, &calculate_factorial, (void *)indx[i]);
        }
    }

    for (i = 0; i < 20; i++)
    {
        pthread_join(t_id[i], NULL);
    }
    ...
```
- ``indexing *indx[20];``: Membuat array indx yang berisi 20 pointer ke struktur indexing. Struktur indexing digunakan untuk menyimpan indeks baris dan kolom dari setiap elemen matriks.
- ``indx[i] = malloc(sizeof(indx));``: Mengalokasikan memori dinamis untuk setiap elemen indx menggunakan malloc. Ukuran alokasi memperhitungkan ukuran dari pointer indx.
- ``indx[i]->i = i;``: Mengisi nilai indeks baris dari indx[i] dengan nilai i.
- ``indx[i]->j = j;``: Mengisi nilai indeks kolom dari indx[i] dengan nilai j.
- ``pthread_create(&t_id[i * 5 + j], NULL, &calculate_factorial, (void *)indx[i]);``: Membuat thread baru menggunakan pthread_create. Setiap thread akan menjalankan fungsi ``calculate_factorial`` dan menerima argumen berupa ``indx[i]``, yaitu pointer ke struktur indexing yang berisi indeks baris dan kolom dari elemen matriks yang akan dihitung faktorialnya.
- ``pthread_join(t_id[i], NULL)``: Memanggil ``pthread_join`` untuk melakukan join terhadap thread dengan ID ``t_id[i]``. ``pthread_join`` akan **memblokir eksekusi thread pemanggil** hingga thread yang ditunggu selesai dieksekusi.

```c
void *calculate_factorial(void *arg)
{
    indexing *indx = (indexing *)arg;
    int i = indx->i;
    int j = indx->j;
    ull factorial = 1;
    for (int k = 1; k <= (*result)[i][j]; k++)
    {
        factorial *= k;
    }
    result_factorial[i][j] = factorial;
}
```
- Pada fungsi ``calculate_factorial``, index i dan j yang tadinya dipassing pada argumen diassign ke variable i dan j pada fungsi. Kemudian, kalkulasi factorial dilakukan dengan mengiterasi perkalian dari 1 hingga nilai pada kolom matrix yang dituju, yang nantinya hasilnya akan disimpan pada ``result_factorial``. 

```c
    ...
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            printf("[%llu] ", result_factorial[i][j]);
        }
        printf("\n");
    }
    ...
```
- Akhirnya, hasil dari factorial pada matrix result diprint.

### Soal D
**Solusi :**
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

#define ull unsigned long long

int (*result)[4][5];
ull result_factorial[4][5];

int main()
{
    clock_t start, end;
    double cpu_time_used;

    start = clock();

    key_t key = 2104;
    int shmid = shmget(key, sizeof(int[4][5]), 0666);
    result = shmat(shmid, NULL, 0);

    int i, j, k;
    printf("\nResult matrix:\n");
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            printf("[%d] ", (*result)[i][j]);
        }
        printf("\n");
    }

    printf("\nHasil faktorial matrix:\n");
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            ull factorial = 1;
            for (k = 1; k <= (*result)[i][j]; k++)
            {
                factorial *= k;
            }
            result_factorial[i][j] = factorial;
        }
    }

    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            printf("[%llu] ", result_factorial[i][j]);
        }
        printf("\n");
    }

    end = clock();
    cpu_time_used = ((double)(end - start)) * 1000 / CLOCKS_PER_SEC;
    printf("\nCPU elapsed time when running: %f ms\n", cpu_time_used);

    return 0;
}
```
**Penjelasan Kode :**
- Pada program diatas, diimplementasikan hal yang sama pada program ``cinta.c``, hanya saja perhitungan faktorial cukup dilakukan dengan looping, tidak dengan teknik multithreading. 
- Perhitungan performa dilakukan dengan menggunakan ``clock()`` yang di start pada awal program dan dihentikan pada akhir program (berlaku juga pada cinta.c). Nantinya, hasil performa antara kedua program akan dibandingkan pada tabel output di bawah.

**Screenshot Output Program Nomor 2 :**
| <p align="center"> Screenshot </p> | <p align="center"> Deskripsi </p> |
| ---------------------------------- | ----------------------------------- |
|<img src="/uploads/fda99287dbf5166d23a2ee5d86e0d777/no2.png" width = "350"/> | <p align="center">Output dari kalian.c</p> |
|<img src="/uploads/a552f7aeb2b43ed6c3cf6557c989efae/2b.png" width = "350"/> | <p align="center">Output dari cinta.c</p> |
|<img src="/uploads/58f496ba6ccd5a908ccf25b6687900d9/no2c.png" width = "350"/> | <p align="center">Output dari sisop.c</p> |

_Dapat dilihat bahwa performa program tanpa multithreading lebih unggul dari yang menerapkan multithreading. Hal ini disebabkan karena adanya waktu yang terbuang pada saat pembuatan thread_.

## Nomor 3
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.

a. Bantulah Elshe untuk membuat sistem stream (_receiver_) **stream.c** dengan _user (multiple sender dengan identifier)_ **user.c** menggunakan **message queue (wajib)**. Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan **semua aktivitas sesuai perintah akan dikerjakan oleh sistem.**

b. _User_ pertama kali akan mengirimkan perintah **DECRYPT** kemudian sistem stream akan melakukan _decrypt/decode/konversi_ pada _file_ **song-playlist.json** sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.
Proses decrypt dilakukan oleh program stream.c tanpa menggunakan koneksi socket sehingga struktur direktorinya adalah sebagai berikut:

|soal 3|
|------|
|playlist.txt|
|song-playlist.json|
|stream.c|
|user.c|

c. Selain itu, _user_ dapat mengirimkan perintah **LIST**, kemudian sistem _stream_ akan menampilkan daftar lagu yang telah di-_decrypt_.

|**Sample Output:**|
|-|
17-MK
1-800273-8255 - Logic
…
Your Love Is My Drug - Kesha
YOUTH - Troye Sivan
ZEZE (feat. Travis Scott & Offset) - Kodak Black

d. _User_ juga dapat mengirimkan perintah **PLAY < SONG >** dengan ketentuan sebagai berikut:

**PLAY "StEreo Heart"**
    
    sistem akan menampilkan: USER <USER_ID> PLAYING "GYM CLASS HEROES - STEREO HEART"

**PLAY "BREAK"**

    sistem akan menampilkan: 
    THERE ARE "N" SONG CONTAINING "BREAK":
    1. THE SCRIPT - BREAKEVEN
    2. ARIANA GRANDE - BREAK FREE
    dengan "N" merupakan banyaknya lagu yang sesuai dengan string query. Untuk contoh di atas berarti THERE ARE "2" SONG CONTAINING "BREAK":

**PLAY "UVUWEVWEVWE"**

    THERE IS NO SONG CONTAINING "UVUWEVWEVWE"

**Untuk mempermudah dan memperpendek kodingan, query bersifat tidak case sensitive**

e. _User_ juga dapat menambahkan lagu ke dalam _playlist_ dengan syarat sebagai berikut:

    1. _User_ mengirimkan perintah

        ADD<SONG1>
        ADD<SONG2>

        sistem akan menampilkan:

        USER <ID_USER> ADD <SONG1>

    2. _User_ dapat mengedit _playlist_ secara bersamaan tetapi lagu yang ditambahkan tidak boleh sama. Apabila terdapat lagu yang sama maka sistem akan meng-_output_-kan “SONG ALREADY ON PLAYLIST”


f. Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan **semaphore (wajib)** untuk membatasi user yang mengakses playlist.
Output-kan **"STREAM SYSTEM OVERLOAD"** pada sistem ketika user ketiga mengirim perintah apapun.

g. Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan **"UNKNOWN COMMAND"**.

**PROGRAM**

1. stream.c
```c
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <unistd.h>

const char *base64_table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

//decode from base64 to string.
char *base64Decoder(const char *input){
    size_t input_len = strlen(input); //count the length of input (string)
    char *output = malloc(((input_len * 3) / 4) + 1); //allocate memory for the output

    //check if memory allocation was successful
    if (output == NULL) {
        return NULL;
    }

    int i = 0, j = 0;

    //loop through the inputstring
    while (i < input_len) {
        //extract each characters from base64
        int sextet_a = input[i] == '=' ? 0 : 
            strchr(base64_table, input[i]) - base64_table;
        int sextet_b = input[i + 1] == '=' ? 0 : 
            strchr(base64_table, input[i + 1]) - base64_table;
        int sextet_c = input[i + 2] == '=' ? 0 : 
            strchr(base64_table, input[i + 2]) - base64_table;
        int sextet_d = input[i + 3] == '=' ? 0 : 
            strchr(base64_table, input[i + 3]) - base64_table;

        //combine sextet_a, sextet_b, sextet_c, sextet_d into a single 24 bit
        int triple = (sextet_a << 18) + (sextet_b << 12) + (sextet_c << 6) + sextet_d;

        //extract each byte from the 24 bit
        output[j++] = (triple >> 16) & 0xFF;

        if (input[i + 2] != '=') {
            output[j++] = (triple >> 8) & 0xFF;
        }

        if (input[i + 3] != '=') {
            output[j++] = triple & 0xFF;
        }

        i += 4;
    }

    output[j] = '\0'; //add null to the end of output
    return output;
}

//decrypt text
char *rot13Decoder(const char *str){
    char *result = malloc(strlen(str) + 1); //allocate the decrypted string

    if (result == NULL) {
        return NULL;
    }

    int i;
    for (i = 0; str[i] != '\0'; i++) {
        if (str[i] >= 'a' && str[i] <= 'm') {
            result[i] = str[i] + 13;
        } else if (str[i] >= 'n' && str[i] <= 'z') {
            result[i] = str[i] - 13;
        } else if (str[i] >= 'A' && str[i] <= 'M') {
            result[i] = str[i] + 13;
        } else if (str[i] >= 'N' && str[i] <= 'Z') {
            result[i] = str[i] - 13;
        } else {
            result[i] = str[i]; //if the characters are not alphabet
        }
    }

    result[i] = '\0';
    return result;
}

//change hexadecimal string into ASCII
char *hexDecoder(const char *input) {
    size_t input_len = strlen(input); //count the length of input

    if (input_len % 2 != 0) { //input_len must genap
        return NULL;
    }

    //allocate memory
    char *output = malloc(input_len / 2 + 1);

    if (output == NULL) { //failed to allocate memory
        return NULL;
    }

    //loop through the input by 2 bytes
    for (int i = 0, j = 0; i < input_len; i += 2, j++) {
        char byte_str[3] = {input[i], input[i + 1], '\0'}; //store 2 bytes
        output[j] = (char)strtol(byte_str, NULL, 16); //convert the byte string into a char
    }

    output[input_len / 2] = '\0';
    return output;
}

void cmd(char *command[]) {
    pid_t pid = fork(); 

    if (pid < 0) { //pid error
        printf("ERROR: fork() failed\n");
        exit(EXIT_FAILURE);
    } else if (pid == 0) { //child process
        //error if execvp returns -1
        if(execvp(command[0], command) == -1) {
            exit(EXIT_FAILURE);
        }
    } else { //pid > 0 = parent process
        int status;
        wait(&status); //wait for the child process
    } 
}

//convert to uppercase
void toUpperCase(char *str) {
    while (*str) {
        *str = toupper(*str);
        str++;
    }
}

void decryptPlaylist() {
    FILE *f = fopen("song-playlist.json", "r"); //open song playlist

    //download from URL
    if (f == NULL) {
        char *url = "https://transfer.sh/get/tN3N7I/song-playlist.json";
        cmd((char *[]){"wget", "-O", "song-playlist.json", "-q", url, NULL});
    }

    //array to hold the file contents
    char fileContent[4096][256] = {""};
    char line[256];
    int i = 0;

    //copy file contents into fileContent[]
    while (fgets(line, sizeof(line), f)) {
        if (strlen(line) > 6) {
            line[strcspn(line, "\n")] = '\0';
            strcpy(fileContent[i++], line);
        }
    }

    //new file for the decrypted playlist
    FILE *out = fopen("playlist.txt", "w");

    if (out == NULL) {
        printf("ERROR: could not create playlist.txt\n");
        exit(1);
    }

    //decoding and writing each song title
    for (int j = 0; j < i; j += 2) {
        //extract the encryption for the current song title
        char *encryptMethod = fileContent[j];
        encryptMethod += 14;
        encryptMethod[strlen(encryptMethod) - 3] = '\0';

        //extract the encoded song title
        char *songTitle = fileContent[j + 1];
        songTitle += 12;
        songTitle[strlen(songTitle) - 2] = '\0';

        //decode the song title
        char *decodedString;
        if (strcmp(encryptMethod, "rot13") == 0) {
            decodedString = rot13Decoder(songTitle);
        } else if (strcmp(encryptMethod, "base64") == 0) {
            decodedString = base64Decoder(songTitle);
        } else if (strcmp(encryptMethod, "hex") == 0) {
            decodedString = hexDecoder(songTitle);
        }

        fprintf(out, "%s\n", decodedString);
    }

    //sort the output alphabetically
    cmd((char *[]){"sort", "playlist.txt", "-o", "playlist.txt", NULL});
}

//search for songs
void playSong(char playlist[1024][128], const char *keyword) {
    int songCount = 0;
    char searchResult[1024][128] = {""};

    //loop through the playlist
    for (int i = 0; i < 1024; i++) {
        //if the playlist is not empty
        if (strcmp(playlist[i], "") != 0) {
            //convert the playlist to uppercase
            char *line = playlist[i];
            toUpperCase(line);
            
            //add the found keyword to searchResult[]
            if (strstr(line, keyword) != NULL) {
                strcpy(searchResult[songCount++], line);
            }
        }
    }

    if (songCount == 0) { //no songs found
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", keyword);
    } else if (songCount == 1) { //only 1 song found
        printf("USER <USER_ID> PLAYING \"%s\"\n", searchResult[0]);
    } else { //more than 1 song found
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\":\n", songCount, keyword);
        
        for (int i = 0; i < songCount; i++) {
            printf("%d. %s\n", i + 1, searchResult[i]);
        }
    }
}

typedef struct Message {
    long msg_type;
    char msg_text[128];
} Message;

int main() {
    char command[128];
    char playlist[1024][128] = {""}, line[128];
    int songCount = 0;

    //open the plalist files
    FILE *f = fopen("playlist.txt", "r");

    if (f != NULL) {
        while (fgets(line, sizeof(line), f)) {
            line[strcspn(line, "\n")] = '\0'; // remove newline
            strcpy(playlist[songCount++], line);
        }
    }

    //create message queue
    key_t key = ftok("msgqueue", 65);
    int msg_id = msgget(key, 0666 | IPC_CREAT);

    while (1) {
        //receive message
        Message msg;
        msgrcv(msg_id, &msg, sizeof(msg), 1, 0);

        char *command = msg.msg_text; //extract the command

        //check the command
        if (strcmp(command, "DECRYPT") == 0) {
            decryptPlaylist();
        } else if (strcmp(command, "LIST") == 0) {
            //print the paylist
            for (int i = 0; i < songCount; i++) {
                printf("%s\n", playlist[i]);
            }
        } else if (strstr(command, "PLAY") != NULL) {
            char *keyword = command;
            keyword += 5; //skip "PLAY"
            toUpperCase(keyword); //convert to uppercase
            playSong(playlist, keyword); //play the song
        } else if (strstr(command, "ADD") != NULL) {
            //extract the song and add it to the playlist
            char *title = command;
            title += 4; //skip "ADD"
            strcpy(playlist[songCount++], title); //copy the song to the playlist
            printf("USER <ID_USER> ADD %s\n", title);
        } else {
            printf("UNKNOWN COMMAND\n");
        }
    }
  return 0;
}
```

**PENJELASAN**

```c
const char *base64_table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
```

- **Pertama**, inisialisasi pointer ke dalam sebuah array ``base64_table``, yang berisi 64 karakter skema **Base64**.

```c
char *base64Decoder(const char *input){
    size_t input_len = strlen(input); //count the length of input (string)
    char *output = malloc(((input_len * 3) / 4) + 1); //allocate memory for the output

    //check if memory allocation was successful
    if (output == NULL) {
        return NULL;
    }

    int i = 0, j = 0;

    //loop through the inputstring
    while (i < input_len) {
        //extract each characters from base64
        int sextet_a = input[i] == '=' ? 0 : 
            strchr(base64_table, input[i]) - base64_table;
        int sextet_b = input[i + 1] == '=' ? 0 : 
            strchr(base64_table, input[i + 1]) - base64_table;
        int sextet_c = input[i + 2] == '=' ? 0 : 
            strchr(base64_table, input[i + 2]) - base64_table;
        int sextet_d = input[i + 3] == '=' ? 0 : 
            strchr(base64_table, input[i + 3]) - base64_table;

        //combine sextet_a, sextet_b, sextet_c, sextet_d into a single 24 bit
        int triple = (sextet_a << 18) + (sextet_b << 12) + (sextet_c << 6) + sextet_d;

        //extract each byte from the 24 bit
        output[j++] = (triple >> 16) & 0xFF;

        if (input[i + 2] != '=') {
            output[j++] = (triple >> 8) & 0xFF;
        }

        if (input[i + 3] != '=') {
            output[j++] = triple & 0xFF;
        }

        i += 4;
    }

    output[j] = '\0'; //add null to the end of output
    return output;
}
```
- **Kedua**, terdapat fungsi untuk melakukan decoding dari string.

- **``size_t input_len = strlen(input)``** digunakan untuk menghitung panjang string input yang akan di-decode.

- **``char *output = malloc(((input_len * 3) / 4) + 1)``** untuk melakukan alokasi memori. Karena setiap 4 karakter _Base64_ merepresentasikan 3 byte, maka untuk menentukan ukuran memori yang dibutuhkan untuk menyimpan hasil decoding, panjang string input dikali dengan 3/4. Karena hasilnya tidak selalu integer, maka dibulatkan ke atas dengan pembulatan **+ 1**.

- **``if (output == NULL) { return NULL; }``** memeriksa apakah alokasi memori berhasil atau tidak.

- **``while (i < input_len) { ... }``** merupakan loop untuk melakukan decoding dari string input.

- **``int sextet_a = input[i] == '=' ? 0 : strchr(base64_table, input[i]) - base64_table``** digunakan untuk mengambil nilai numerik dari karakter pertama dalam _Base64_. Jika karakter tersebut adalah karakter '=', maka nilai numeriknya adalah 0. Jika bukan, maka nilai numeriknya dihitung berdasarkan posisinya dalam tabel ``base64_table``.

- **``int sextet_b = ...; int sextet_c = ...; int sextet_d = ...``** digunakan untuk mengambil nilai numerik dari karakter-karakter berikutnya dalam _Base64_. Jika karakter '=', maka nilai numeriknya adalah 0. Jika bukan, maka nilai numeriknya dihitung berdasarkan posisinya dalam tabel ``base64_table``.

- **``int triple = (sextet_a << 18) + (sextet_b << 12) + (sextet_c << 6) + sextet_d``** menggabungkan 4 nilai numerik dari karakter pada _Base64_ menjadi sebuah integer 24 bit.

- **``output[j++] = (triple >> 16) & 0xFF``** untuk mengambil byte pertama dari integer 24 bit dan menyimpannya ke dalam variabel ``output`` pada indeks ke-j.

- **``if (input[i + 2] != '=') {...}``** memeriksa jika karakter ketiga dalam _Base64_ bukan karakter '=', maka program akan mengekstrak setiap byte dari 24 bit.

- **``if (input[i + 3] != '=') {...}``** memeriksa jika karakter keempat adalah "=", makaa byte ke-3 tidak perlu di-_decode_ dan output array hanya akan diisi dengan byte pertama dan kedua dari 24 bit.




```c
char *rot13Decoder(const char *str){
    char *result = malloc(strlen(str) + 1); //allocate the decrypted string

    if (result == NULL) {
        return NULL;
    }

    int i;
    for (i = 0; str[i] != '\0'; i++) {
        if (str[i] >= 'a' && str[i] <= 'm') {
            result[i] = str[i] + 13;
        } else if (str[i] >= 'n' && str[i] <= 'z') {
            result[i] = str[i] - 13;
        } else if (str[i] >= 'A' && str[i] <= 'M') {
            result[i] = str[i] + 13;
        } else if (str[i] >= 'N' && str[i] <= 'Z') {
            result[i] = str[i] - 13;
        } else {
            result[i] = str[i]; //if the characters are not alphabet
        }
    }

    result[i] = '\0';
    return result;
}
```
- **Ketiga**, definisikan suatu fungsi untuk melakukan dekripsi teks yang dienkripsi dengan ``ROT13``.

- **``char *result = malloc(strlen(str) + 1); //allocate the decrypted string``** melakukan alokasi memori.

- **``if (result == NULL) {return NULL;}``** memerika apakah alokasi memori berhasil atau tidak.

- **``for (i = 0; str[i] != '\0'; i++) {...}``** Memeriksa apakah karakter berada dalam rentang karakter 'a' hingga 'm' atau dari karakter 'A' hingga 'M'. Jika iya, maka karakter tersebut digeser 13 posisi ke depan dalam urutan abjad. Jika tidak, maka pengecekan dilakukan untuk karakter 'n' hingga 'z' dan 'N' hingga 'Z'. Jika karakter berada dalam salah satu rentang tersebut, maka karakter tersebut digeser 13 posisi ke belakang.




```c
char *hexDecoder(const char *input) {
    size_t input_len = strlen(input); //count the length of input

    if (input_len % 2 != 0) { //input_len must genap
        return NULL;
    }

    //allocate memory
    char *output = malloc(input_len / 2 + 1);

    if (output == NULL) { //failed to allocate memory
        return NULL;
    }

    //loop through the input by 2 bytes
    for (int i = 0, j = 0; i < input_len; i += 2, j++) {
        char byte_str[3] = {input[i], input[i + 1], '\0'}; //store 2 bytes
        output[j] = (char)strtol(byte_str, NULL, 16); //convert the byte string into a char
    }

    output[input_len / 2] = '\0';
    return output;
}
```
- **Keempat**, definisikan fungsi untuk mengonversi input dalam bentuk _hexadecimal_ menjadi output dalam bentuk karakter.

- **``size_t input_len = strlen(input)``** mengecek panjang string input.

- **``if (input_len%2 != 0) {return NULL;}``** memeriksa jika input bukan bilangan genap, maka fungsi akan me-return ``NULL``.

- **`` char *output = malloc(input_len / 2 + 1);``** melakukan alokasi memori untuk variabel ``output`` menggunakan fungsi ``malloc()``. Ukuran alokasi dihitung dengan membagi panjang input dengan 2 dan ditambah 1 untuk karakter null terminator.

- **``if (output == NULL) {return NULL;}``** memerika  Jika alokasi gagal, maka fungsi akan me-return nilai ``NULL``.

- **``for(int i=0, j=0; i<input_len; i+=2, j++){...}``** melakukan looping untuk setiap 2 karakter pada input menggunakan variabel ``i`` dan ``j``. Lalu, terdapat array ``byte_str`` yang berisi karakter pada indeks "i" dan "i+1" pada input. Kemudian, dilakukan konversi string _hexadecimal_ menjadi karakter menggunakan ``strtol()``. Hasilnya kemudian disimpan pada elemen ke-j pada variabel ``output``.




```c
void cmd(char *command[]) {
    pid_t pid = fork(); 

    if (pid < 0) { //pid error
        printf("ERROR: fork() failed\n");
        exit(EXIT_FAILURE);
    } else if (pid == 0) { //child process
        //error if execvp returns -1
        if(execvp(command[0], command) == -1) {
            exit(EXIT_FAILURE);
        }
    } else { //pid > 0 = parent process
        int status;
        wait(&status); //wait for the child process
    } 
}
```
- **Kelima**, didefinisikan suatu fungsi  untuk untuk menerima _command_ dari _user_.

- **``pid_t pid = fork();``** akan membuat _child process_ menggunakan ``fork()``.

- **``if(pid<0) {...}``** memeriksa jika nilai ``pid`` kurang dari 0, maka menandakan bahwa proses error.

- **``else if (pid == 0) {...}``** memeriksa jika ``fork()`` sama dengan 0, maka program berada dalam _child process_. Program akan menjalankan _command_ yang diberikan dengan memanggil ``execvp()`` yang akan menggantikan konten memori dari _child process_ dengan program yang dijalankan oleh _command_. Jika ``execvp()`` me-return nilai -1, makaterjadi error saat menjalankan _command_ sehingga program akan keluar dari _child process_ dengan memanggil fungsi ``exit()``.

- Jika ``fork()`` lebih besar dari 0, maka program berada dalam _parent process_. Program akan menunggu _child process_ selesai dengan fungsi ``wait()`` yang akan memblokir _parent process_ sampai _child process_ selesai dijalankan.




```c
void toUpperCase(char *str) {
    while (*str) {
        *str = toupper(*str);
        str++;
    }
}
```
- **Keenam**, buat fungsi untuk mengkonversi string menjadi _uppercase_ atau hutuf kapital.


```c
void decryptPlaylist(){
    FILE *f = fopen("song-playlist.json", "r");

    if (f == NULL) {
        char *url = "https://transfer.sh/get/tN3N7I/song-playlist.json";
        cmd((char *[]){"wget", "-O", "song-playlist.json", "-q", url, NULL});
    }

    char fileContent[4096][256] = {""};
    char line[256];
    int i = 0;

    while (fgets(line, sizeof(line), f)) {
        if (strlen(line) > 6) {
            line[strcspn(line, "\n")] = '\0';
            strcpy(fileContent[i++], line);
        }
    }

    FILE *out = fopen("playlist.txt", "w");

    if (out == NULL) {
        printf("ERROR: could not create playlist.txt\n");
        exit(1);
    }

    for (int j = 0; j < i; j += 2) {
        char *encryptMethod = fileContent[j];
        encryptMethod += 14;
        encryptMethod[strlen(encryptMethod) - 3] = '\0';

        char *songTitle = fileContent[j + 1];
        songTitle += 12;
        songTitle[strlen(songTitle) - 2] = '\0';

        char *decodedString;
        if (strcmp(encryptMethod, "rot13") == 0) {
            decodedString = rot13Decoder(songTitle);
        } else if (strcmp(encryptMethod, "base64") == 0) {
            decodedString = base64Decoder(songTitle);
        } else if (strcmp(encryptMethod, "hex") == 0) {
            decodedString = hexDecoder(songTitle);
        }

        fprintf(out, "%s\n", decodedString);
    }

    cmd((char *[]){"sort", "playlist.txt", "-o", "playlist.txt", NULL});
}
```

- **Ketujuh**, deklarasikan suatu fungsi untuk mendekripsi file ``song-plaulist.json.``

- **``FILE *f = fopen("song-playlist.json", "r");``** membuka file ``song-playlist.json`` menggunakan ``fopen``, kemudian menyimpan file tersebut pada pointer ``f``.

- **``if (f == NULL) {...}``** memeriksa Jika file tidak dapat ditemukan, maka program akan melakukan _download_ file dengan menggunakan fungsi ``cmd`` yang akan menjalankan ``wget``. Jika berhasil, file akan disimpan dengan nama ``song-playlist.json``.

- **``char fileContent[4096][256] = {""}; char line[256]; int i = 0;``** membuat sebuah array ``fileContent``, serta variabel ``line`` yang akan digunakan untuk membaca setiap baris dari file ``song-playlist.json``. Variabel ``i`` digunakan untuk menghitung jumlah baris yang berhasil dibaca.

- **``while(fgets(line, sizeof(line), f)){...}``** melakukan _looping_ untuk membaca setiap baris dari file ``song-playlist.json`` menggunakan ``fgets``.

- **``if(strlen(line)>6){...}``** memeriksa jika panjang ``strlen`` lebih dari 6 karakter, maka akan disimpan ke dalam ``fileContent``.

- **``FILE *out = fopen("playlist.txt", "w");``** membuka file ``playlist.txt`` kemudian menyimpan file tersebut pada pointer ``out``.

- **``if(out == NULL) {...}``** menandakan error jikak file ``playlist.txt`` tidak dapat dibuat.

- **``for(int j=0; j<i; j+=2){...}``** file playlist akan dibaca. Setiap 2 baris akan dianggap sebagai 1 lagu. Pertama, pada baris dengan indeks ganjil akan diambil enkripsi pada string yang berisi nama metode enkripsi. Selanjutnya, pada baris dengan indeks genap akan diambil nama lagu yang terenkripsi pada string yang berisi nama lagu terenkripsi. Kemudian, nilai dari string akan dijadikan argumen untuk memanggil fungsi dekripsi yang sesuai, yaitu fungsi ``rot13Decoder()``, ``base64Decoder()``, atau ``hexDecoder()``.
Hasil dekripsi kemudian akan ditulis ke file ``playlist.txt`` dan loop dilanjutkan untuk membaca baris berikutnya.

- **``cmd((char *[]){"sort", "playlist.txt", "-o", "playlist.txt", NULL});``** terdapat fungsi ``cmd`` yang akan menjalankan _command_ yang ada pada parameter tersebut di terminal. _Command_ yang dijalankan adalah ``sort -o`` untuk mengurutkan isi file ``playlist.txt`` dan menyimpan hasilnya ke dalam file ``playlist.txt``.





```c
void playSong(char playlist[1024][128], const char *keyword) {
    int songCount = 0;
    char searchResult[1024][128] = {""};

    //loop through the playlist
    for (int i = 0; i < 1024; i++) {
        //if the playlist is not empty
        if (strcmp(playlist[i], "") != 0) {
            //convert the playlist to uppercase
            char *line = playlist[i];
            toUpperCase(line);
            
            //add the found keyword to searchResult[]
            if (strstr(line, keyword) != NULL) {
                strcpy(searchResult[songCount++], line);
            }
        }
    }

    if (songCount == 0) { //no songs found
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", keyword);
    } else if (songCount == 1) { //only 1 song found
        printf("USER <USER_ID> PLAYING \"%s\"\n", searchResult[0]);
    } else { //more than 1 song found
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\":\n", songCount, keyword);
        
        for (int i = 0; i < songCount; i++) {
            printf("%d. %s\n", i + 1, searchResult[i]);
        }
    }
}
```
- **Kedelapan**, deklarasikan fungsi untuk mencari lagu pada sebuah playlist berdasarkan _keyword_ dari _user_.

- Fungsi ini akan menginisialisasi variabel ``songCount`` dan ``searchResult`` dengan nilai awal 0 dan string kosong.

- Selanjutnya, program akan melakukan _loop_ sebanyak 1024 kali untuk mencari lagu yang mengandung keyword yang diberikan. Setiap lagu yang ditemukan akan ditambahkan ke dalam ``searchResult[]`` dan variabel ``songCount`` akan bertambah.

- Jika tidak ada lagu yang ditemukan, maka program akan mencetak "THERE IS NO SONG CONTAINING".

- Jika hanya ada satu lagu yang ditemukan, maka program akan mencetak "USER <USER_ID> PLAYING".

- Jika lebih dari satu lagu ditemukan, maka program akan mencetak "THERE ARE" dan menampilkan jumlah lagu yang ditemukan serta daftar lagu yang mengandung keyword tersebut beserta nomor urutannya.




```c
typedef struct Message {
    long msg_type;
    char msg_text[128];
} Message;
```

- **Kesembilan**, buat ``struct`` untuk variabel ``msg_type`` dan array ``msg_text[]``.



```c
int main() {
    char command[128];
    char playlist[1024][128] = {""}, line[128];
    int songCount = 0;

    //open the plalist files
    FILE *f = fopen("playlist.txt", "r");

    if (f != NULL) {
        while (fgets(line, sizeof(line), f)) {
            line[strcspn(line, "\n")] = '\0'; // remove newline
            strcpy(playlist[songCount++], line);
        }
    }

    //create message queue
    key_t key = ftok("msgqueue", 65);
    int msg_id = msgget(key, 0666 | IPC_CREAT);

    while (1) {
        //receive message
        Message msg;
        msgrcv(msg_id, &msg, sizeof(msg), 1, 0);

        char *command = msg.msg_text; //extract the command

        //check the command
        if (strcmp(command, "DECRYPT") == 0) {
            decryptPlaylist();
        } else if (strcmp(command, "LIST") == 0) {
            //print the paylist
            for (int i = 0; i < songCount; i++) {
                printf("%s\n", playlist[i]);
            }
        } else if (strstr(command, "PLAY") != NULL) {
            char *keyword = command;
            keyword += 5; //skip "PLAY"
            toUpperCase(keyword); //convert to uppercase
            playSong(playlist, keyword); //play the song
        } else if (strstr(command, "ADD") != NULL) {
            //extract the song and add it to the playlist
            char *title = command;
            title += 4; //skip "ADD"
            strcpy(playlist[songCount++], title); //copy the song to the playlist
            printf("USER <ID_USER> ADD %s\n", title);
        } else {
            printf("UNKNOWN COMMAND\n");
        }
    }
  return 0;
}
```
- **Bagian terakhir**, program membaca playlist dari file ``playlist.txt`` dan menyimpan setiap baris lagu di array ``playlist``. Kemudian, program membuat _message queue_ dengan menggunakan ``ftok()`` dan ``msgget()``.

- Selanjutnya, program akan terus menerima pesan menggunakan fungsi ``msgrcv()``. Pesan yang diterima berisi perintah untuk melakukan suatu perintah pada playlist.

- Jika perintah adalah **DECRYPT**, maka program akan memanggil fungsi ``decryptPlaylist()`` untuk mendekripsi playlist. 

- Jika perintah adalah **LIST**, maka program akan mencetak seluruh lagu dalam playlist. 

- Jika perintah adalah **PLAY**, maka program akan memanggil fungsi ``playSong()`` untuk memutar lagu. 

- Jika perintah adalah **ADD**, maka program akan menambahkan lagu ke playlist dan mencetak ``USER <ID_USER> ADD <song_title>``. 

- Jika perintah tidak dikenali, program akan mencetak ``UNKNOWN COMMAND``.



2. user.c 

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>

typedef struct Message {
	long msg_type;
	char msg_text[128];
} Message;

int main() {
	key_t key = ftok("msgqueue", 65);
	int msg_id = msgget(key, 0666 | IPC_CREAT);
	
	Message msg;
  	msg.msg_type = 1;
	
  	while (1) {
    	printf("COMMAND: ");
    	scanf(" %[^\n]", msg.msg_text);
		
    	msgsnd(msg_id, &msg, sizeof(msg), 0);
  	}

  	return 0;
}
```

- Untuk file kedua, yaitu ``user.c`` terdiri dari 2 bagian.

- **Pertama**, terdapat _struct_ yang berisi variabel ``msg_type`` untuk menyimpan tipe pesan dan array karakter ``msg_text[128]`` untuk menyimpan isi pesan.

- Selanjutnya, program akan membuat _message queue_ menggunakan ``ftok()`` untuk menghasilkan key dan ``msgget()`` untuk membuat _message queue_ dengan menggunakan key tersebut.

- Setelah itu, program akan meminta input _command_ dari _user_  dan menyimpannya pada ``msg_text``. Kemudian, program akan mengirim pesan tersebut ke _message queue_ menggunakan ``msgsnd``.

**Screenshot Output Program Nomor 3 :**
| <p align="center"> User </p> | <p align="center"> Stream </p> |
| ---------------------------------- | ----------------------------------- |
|<img src="/uploads/ac32db02aa0b0af03b0e818a0caff50f/no3.png" width = "350"/> | <img src="/uploads/f5367397a47c76889437dd76b13383fe/no3b.png" width = "350"/> |
|<img src="/uploads/6b50a1207670eedfeccc73b0ab11982c/no3d.png" width = "350"/> | <img src="/uploads/88ed359f87a2c98da25db1d34c6c3c52/no3c.png" width = "350"/> |
|<img src="/uploads/d87c559de1a98b9406e52863b642e7ba/no3e.png" width = "350"/> | <img src="/uploads/d17d59217898f99cdcb09550bcc64886/no3f.png" width = "350"/> |
|<img src="/uploads/814cf243634960addf238380a7cfb2ae/no3g.png" width = "350"/> | <img src="/uploads/dfe2e7c84fddcb59842ae291ab8940a4/no3h.png" width = "350"/> |
|<img src="/uploads/d6eea2eaea2242f92335b46119a795af/no3j.png" width = "350"/> | <img src="/uploads/2c7f8509ef0540da2bed2ed652b01316/no3i.png" width = "350"/> |


## Nomor 4
Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama _hehe.zip_. Di dalam file .zip tersebut, terdapat sebuah folder bernama **files** dan _file.txt_ bernama **extensions.txt** dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan _file_ tersebut!

a. _Download_ dan _unzip file_ tersebut dalam kode c bernama **unzip.c**.

b. Selanjutnya, buatlah program **categorize.c** untuk mengumpulkan (move / copy) file sesuai _extension_-nya. _Extension_ yang ingin dikumpulkan terdapat dalam file **extensions.txt**. Buatlah folder **categorized** dan di dalamnya setiap _extension_ akan dibuatkan _folder_ dengan nama sesuai nama _extension_-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua _lowercase_. _File_ lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder **other**.
Pada _file_ **max.txt**, terdapat angka yang merupakan isi maksimum dari _folder_ tiap _extension_ kecuali _folder_ **other**. Sehingga, jika penuh, buatlah _folder_ baru dengan format **extension (2), extension (3)**, dan seterusnya.

c. Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.

    a. extension_a : banyak_file
    b. extension_b : banyak_file
    c. extension_c : banyak_file
    d. other : banyak_file

d. Setiap pengaksesan _folder, sub-folder_, dan semua folder pada _program categorize.c_ **wajib menggunakan multithreading**. Jika tidak menggunakan akan ada pengurangan nilai.

e. Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.

- **DD-MM-YYY HH:MM:SS ACCESSED [folder path]**
- **DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]**
- **DD-MM-YYYY HH:MM:SS MOVED [folder name]**

examples :
- 02-05-2023 10:01:02 ACCESSED files
- 02-05-2023 10:01:03 ACCESSED files/abcd
- 02-05-2023 10:01:04 MADE categorized
- 02-05-2023 10:01:05 MADE categorized/jpg
- 02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg

Catatan:
    - Path dimulai dari folder files atau categorized
    - Simpan di dalam log.txt
    - ACCESSED merupakan folder files beserta dalamnya
    - Urutan log tidak harus sama

f. Untuk mengecek apakah _log_-nya benar, buatlah suatu program baru dengan nama **logchecker.c** untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.

    - Untuk menghitung banyaknya ACCESSED yang dilakukan.
    - Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
    - Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.

### Soal A
**Solusi :**
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <dirent.h>

int main()
{
    pid_t pid;
    int status;
    pid = fork();

    if (pid < 0) {
        perror("fork");
    }
    if(pid == 0) {
        execlp("wget", "wget", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL);
        exit(0);
    } else {
        waitpid(pid, &status, 0);
        pid = fork();
        
        if (pid < 0) {
            perror("fork");
        }
        if (pid == 0) {
            execlp("unzip", "unzip", "hehe.zip", NULL);
        }
    }
    return 0;
}
```
**Penjelasan Kode :**
- Pada program ``unzip.c`` di atas, dibuat parent-child process dimana child process melakukan download zip file yang diminta soal, kemudian parentnya akan menunggu hingga download tersebut selesai dan nantinya akan mengspawn child process lainnya untuk melakukan proses unzip.

### Soal B-E
**Solusi :**
```c
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pthread.h>
#include <ctype.h>
#include <math.h>

#define MAX_LEN 256
#define MAX_PATH 256
#define MAX_EXT 5
#define MAX_DEST_SIZE 475 // maximum size of destination buffer

pthread_mutex_t lock;
FILE *log_file;

struct ThreadArgs
{
    char dirname[MAX_LEN];
    int maxFile;
};

void writeLogMade(char createdDir[])
{
    // menulis pesan log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logMADE[256] = "MADE ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMADE, createdDir);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logMADE);
    pthread_mutex_unlock(&lock);
}

void writeLogAccess(char folderPath[])
{
    // menulis pesan log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logAccess[256] = "ACCESSED ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logAccess, folderPath);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logAccess);
    pthread_mutex_unlock(&lock);
}

void *create_directory(void *arg)
{
    struct ThreadArgs *thread_args = (struct ThreadArgs *)arg;
    char *dirname = thread_args->dirname;
    int maxFile = thread_args->maxFile;
    char ext[4];
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/";
    char new_dirname[MAX_LEN];
    struct stat st;

    // membuat nama direktori baru dengan menambahkan prefix "categorized/"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);

    // hitung banyak file
    char countFile[256];
    char hehe_dir[20] = "files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    // Mengeksekusi command dan membuka stream untuk membaca outputnya
    FILE *fp;
    fp = popen(cmd, "r");
    if (fp == NULL)
    {
        printf("Error: Failed to execute command.\n");
    }

    // Membaca output dari stream dan menampilkannya ke layar
    writeLogAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    // ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);

    // menghitung total direktori yang dibuat per ext
    int numDir;
    if (maxFile == 0)
    {
        numDir = 1;
    }
    else
    {
        numDir = numFiles / maxFile;
        if (numFiles % maxFile > 0)
            numDir = numDir + 1;
    }

    // Menutup stream dan mengembalikan nilai 0 (sukses)
    pclose(fp);

    // create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0)
    {
        // directory creation was successful, so do something
        writeLogMade(new_dirname);
    }

    for (int i = 0; i < numDir; i++)
    {
        // mendefinisikan variable nama directory dengan suffix sesuai dengan index
        char _dirName[30];
        strcpy(_dirName, new_dirname);
        if (i > 0)
        {
            char suffix[10];
            sprintf(suffix, " (%d)", i + 1);
            strcat(_dirName, suffix);
        }

        // memeriksa apakah direktori sudah ada
        if (stat(_dirName, &st) == 0)
        {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        }
        else
        {
            // membuat direktori jika belum ada
            if (mkdir(_dirName, 0777) == 0)
            {
                writeLogMade(_dirName);
                // move files according to their extensions
                char command1[600];
                char command2[500];
                char command3[100];
                char command4[600];
                char logAccessSource[200];
                char logAccessTarget[100];

                sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);

                system(logAccessSource);
                system(logAccessTarget);

                sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

                if (maxFile == 0)
                {
                    sprintf(command4, "find %s/ -type f -iname \"*.%s\" |xargs -I {} sh -c '%s' ", hehe_dir, ext, command2);
                    system(command4);
                }
                else
                {
                    sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2);

                    system(command1);
                }
            }
            else
            {
                perror("Gagal membuat direktori");
            }
        }
    }
    return NULL;
}

int main()
{
    // membuka file log.txt
    log_file = fopen("log.txt", "a");
    if (log_file == NULL)
    {
        perror("Failed to open log file");
        exit(1);
    }

    // inisialisasi mutex lock
    if (pthread_mutex_init(&lock, NULL) != 0)
    {
        perror("Failed to initialize mutex lock");
        exit(1);
    }

    char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    struct ThreadArgs thread_args[MAX_LEN];
    int i = 0;

    // membuat direktori "categorized" jika belum ada
    if (stat("categorized", &st) != 0)
    {
        mkdir("categorized", 0777);
        writeLogMade("categorized");
    }

    // membaca file max.txt dan menyimpan nilainya
    char maxfilename[50] = "max.txt";
    // open max.txt
    FILE *max;
    // membuka file untuk dibaca
    max = fopen(maxfilename, "r");

    // memeriksa apakah file berhasil dibuka
    if (max == NULL)
    {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // read the first line of max.txt into firstLine

    // menutup file max.txt
    fclose(max);

    // membuka file extensions.txt untuk dibaca
    fp = fopen(filename, "r");

    // memeriksa apakah file extensions.txt berhasil dibuka
    if (fp == NULL)
    {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    // membaca nama direktori dari file, satu per satu
    while (fgets(dirname, MAX_LEN, fp))
    {
        // menghilangkan karakter newline di akhir string
        dirname[strcspn(dirname, "\n")] = 0;

        // menghapus spasi di akhir string
        size_t len = strlen(dirname);
        while (len > 0 && isspace(dirname[len - 1]))
        {
            len--;
        }
        dirname[len] = '\0';

        // membuat direktori lain secara asynchronous dengan menggunakan thread
        strcpy(thread_args[i].dirname, dirname);
        thread_args[i].maxFile = maxFile;
        pthread_create(&tid[i], NULL, create_directory, &thread_args[i]);
        i++;
    }

    // join thread yang masih berjalan
    for (int j = 0; j < i; j++)
    {
        pthread_join(tid[j], NULL);
    }

    // membuat direktori "other" di dalam "categorized" jika belum ada
    if (stat("categorized/other", &st) != 0)
    {
        writeLogMade("categorized/other");
        if (mkdir("categorized/other", 0777) != 0)
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n");
    }

    // move other files to categorized/other
    char cmdOther1[600];
    char cmdOther2[500];
    char cmdOther3[100];
    char otherPath[20] = "categorized/other";
    char logAccessSource[200];
    char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);

    system(logAccessSource);
    system(logAccessTarget);

    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3);
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2);
    system(cmdOther1);

    // output sort buffer.txt untuk jumlah file tiap extension
    system("sort buffer.txt");
    system("rm -rf buffer.txt");

    // count how many files in dir categorized/other
    DIR *dir;
    struct dirent *ent;
    int count = 0;
    if ((dir = opendir(otherPath)) != NULL)
    {
        writeLogAccess(otherPath);
        while ((ent = readdir(dir)) != NULL)
        {
            if (ent->d_type == DT_REG)
            {
                count++;
            }
        }
        closedir(dir);
    }
    else
    {
        perror("Tidak bisa membuka direktori");
        return 1;
    }
    printf("other : %d\n", count);

    // menutup file
    fclose(fp);

    // menghancurkan mutex lock
    pthread_mutex_destroy(&lock);

    // menutup file log
    fclose(log_file);

    return 0;
}
```
Berikut adalah alur yang kami gunakan dalam merancang program di atas:
1. Membuat direktori "categorized/".
2. Membuat struct untuk argumen yang diperlukan.
3. Membaca file "max.txt" dan menyimpan nilainya.
4. Membaca file "extension.txt" untuk mendapatkan daftar ekstensi file yang akan dikategorikan.
5. Membuat fungsi untuk menulis log
6. Membuat dan mengisi sub-direktori sesuai dengan extension yang terdapat pada file "extension.txt".
    - Membuat multithreading untuk mengatur pembuatan direktori pada setiap extension.
    - Untuk setiap extension, menghitung jumlah file yang memiliki ekstensi tersebut dengan menggunakan command "find" dan "wc -l", kemudian membagi jumlah file tersebut dengan nilai yang terdapat pada file "max.txt" untuk menentukan jumlah direktori yang harus dibuat.
    - Membuat direktori baru sesuai dengan format yang diminta pada soal dan menuliskan pesan log ke dalam file log.txt.
Memindahkan file-file yang memiliki ekstensi tersebut ke dalam direktori yang sesuai dengan ekstensinya dan menuliskan pesan log ke dalam file log.txt.
7. Menggabungkan thread yang masih berjalan.
8. Membuat sub-direktori "other" di dalam direktori "categorized"
9. Mengisi sub-direktori "other" untuk file dengan ekstensi selain yang tercantum pada file "extension.txt" dan menuliskan pesan log ke dalam file log.txt.
10. Mengeluarkan pada terminal banyak file tiap extension secara ascending beserta banyak file sub-direktori other

#### 1. Membuat direktori "categorized/"
Untuk membuat direktori "categorized/", kami menggunakan kode berikut:

```c
// membuat direktori "categorized" jika belum ada
    if (stat("categorized", &st) != 0) {
        mkdir("categorized", 0777);
        writeLogMade("categorized");
    }
```

Kode tersebut akan memeriksa apakah direktori "categorized" sudah ada atau belum. Jika belum ada, maka kode akan membuat direktori tersebut dengan menggunakan fungsi `mkdir()`. Selain itu, kode juga akan menuliskan pesan log menggunakan fungsi `writeLogMade()` yang mencatat bahwa direktori "categorized" telah berhasil dibuat. 

`stat("categorized", &st) != 0` digunakan untuk mengecek apakah direktori "categorized/" sudah ada atau belum pada current working directory. Fungsi `stat` digunakan untuk mendapatkan informasi mengenai file atau direktori, seperti ukuran, waktu modifikasi, owner, dan permission. Jika nilai yang dikembalikan oleh stat bukan 0, artinya direktori tersebut belum ada, maka proses pembuatan direktori dilanjutkan.

`mkdir("categorized", 0777)` digunakan untuk membuat direktori "categorized/" dengan mode permission 0777. Mode permission 0777 berarti direktori dapat diakses oleh semua pengguna, termasuk membaca, menulis, dan menjalankan file di dalamnya.

`writeLogMade("categorized")` digunakan untuk menuliskan pesan log ke dalam file log.txt dengan menggunakan fungsi `writeLogMade()`. Pesan log tersebut akan mencatat bahwa direktori "categorized/" telah dibuat dan ditambahkan ke dalam file log.txt sebagai log.

#### 2. Membuat struct untuk argumen yang diperlukan
Struct yang kami buat adalah sebagai berikut:

```c
struct ThreadArgs {
    char dirname[MAX_LEN];
    int maxFile;
};
```

Kode tersebut mendefinisikan sebuah struct bernama `ThreadArgs` yang memiliki dua variabel anggota, yaitu `dirname` dan `maxFile`. Variabel `dirname` merupakan sebuah array karakter dengan ukuran `MAX_LEN` yang menyimpan nama direktori yang akan dibuat.

Variabel `maxFile` adalah sebuah bilangan bulat yang menunjukkan jumlah file maksimum dalam satu direktori. Struct ini kemudian digunakan sebagai argumen untuk thread pada tahap pembuatan direktori untuk setiap ekstensi.

#### 3. Membaca file "max.txt" dan menyimpan nilainya
Kami menggunakan kode berikut untuk membaca file "max.txt" dan juga menyimpan nilainya.

```c
char maxfilename[50] = "max.txt";
    //open max.txt
    FILE *max;
    // membuka file untuk dibaca
    max = fopen(maxfilename, "r");

    // memeriksa apakah file berhasil dibuka
    if (max == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // read the first line of max.txt into firstLine

     // menutup file
    fclose(max);
```
Variabel `maxfilename` didefinisikan sebagai string yang berisi nama file "max.txt". Kemudian, menggunakan fungsi `fopen()`, file "max.txt" dibuka dalam mode "r" (read) dan pointer ke file tersebut disimpan dalam variabel `max`.

Setelah file berhasil dibuka, program membaca nilai integer pertama pada file "max.txt" menggunakan fungsi `fscanf()` dan menyimpannya dalam variabel `maxFile`. Nilai ini digunakan nanti untuk menghitung jumlah direktori yang akan dibuat.

Terakhir, file ditutup menggunakan fungsi `fclose()`. Jika file gagal dibuka (misalnya jika file tidak ada di direktori yang sama dengan program), maka akan ditampilkan pesan "File tidak dapat dibuka." dan program akan dihentikan menggunakan fungsi exit(1).

#### 4. Membaca file "extension.txt" untuk mendapatkan daftar ekstensi file yang akan dikategorikan
Berikut ini adalah kode yang kami buat untuk tahapan ini:

```c
char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    struct ThreadArgs thread_args[MAX_LEN];
    int i = 0;

    // membuka file untuk dibaca
    fp = fopen(filename, "r");

    // memeriksa apakah file berhasil dibuka
    if (fp == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }
```
Kode tersebut adalah bagian dari program untuk membaca file "extensions.txt" yang berisi daftar ekstensi file yang akan dikategorikan. 

Adapun penjelasan dari kode tersebut adalah sebagai berikut:
- `char filename[50] = "extensions.txt";` 

    Sebuah string yang berisi nama file yang akan dibaca.

- `char dirname[MAX_LEN];`

    Sebuah string yang akan digunakan untuk menyimpan nama direktori sesuai dengan ekstensi file yang akan dibuat.

- `FILE *fp;`  

    Pointer untuk file yang akan dibaca.

- `struct stat st;` 

    Sebuah struct yang digunakan untuk mengecek status dari file "categorized/".

- `pthread_t tid[MAX_LEN];`

    Array of thread yang akan digunakan untuk membuat direktori pada setiap ekstensi.

- `struct ThreadArgs thread_args[MAX_LEN];` 

    Array of struct yang berisi argumen untuk setiap thread.

-  `int i = 0;` 

    Sebuah variabel integer yang akan digunakan sebagai counter dalam looping.

Selanjutnya, program akan membuka file "extensions.txt" dengan menggunakan fungsi `fopen() `dengan mode "r" (read-only). Kemudian, program akan memeriksa apakah file berhasil dibuka atau tidak dengan menggunakan kondisi `if (fp == NULL)`. Jika file tidak dapat dibuka, maka program akan menampilkan pesan kesalahan "File tidak dapat dibuka." dan keluar dari program dengan menggunakan fungsi exit(1).

#### 5. Membuat fungsi untuk menulis log
Untuk menulis log ke dalam file **log.txt** format penamaan yang harus diikuti adalah sebagai berikut:

**DD-MM-YYYY HH:MM:SS ACCESSED [folder path]**

**DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]**

**DD-MM-YYYY HH:MM:SS MADE [folder name]**

Maka, terdapat tiga jenis log yang harus dibuat yaitu saat akses, memindahkan file, dan membuat folder. Sebelum menulis log ke dalam "log.txt", kita harus membuat **mutual exclusion**, sebab program yang kami buat menggunakan multithreading sehingga dapat terjadi **race condition** yang dapat mengakibatkan program menjadi tidak stabil atau menghasilkan output yang salah. Berikut ini adalah code mutex yang kami buat di dalam `main`:

```c
 // membuka file log
    log_file = fopen("log.txt", "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
        exit(1);
    }

    // inisialisasi mutex lock
    if (pthread_mutex_init(&lock, NULL) != 0) {
        perror("Failed to initialize mutex lock");
        exit(1);
    }
```

Kode tersebut merupakan inisialisasi untuk membuka file log dan menginisialisasi **mutex lock** pada program.

Pada bagian pertama, fungsi `fopen()` digunakan untuk membuka file log.txt dengan mode "a", yaitu mode untuk menambahkan isi file baru pada akhir file yang sudah ada, sehingga log dari setiap proses yang dilakukan akan ditambahkan pada akhir file log.txt yang sudah ada. Jika file tidak berhasil dibuka, maka program akan keluar dan menampilkan pesan error.

Pada bagian kedua, fungsi `pthread_mutex_init()` digunakan untuk menginisialisasi mutex lock pada program. Mutex lock digunakan untuk mengatur akses ke bagian program yang sama agar tidak dilakukan akses secara bersamaan oleh beberapa thread pada saat yang sama. Jika gagal melakukan inisialisasi mutex lock, maka program akan keluar dan menampilkan pesan error.

##### **5.1. log MADE**
Untuk membuat log MADE, kami membuat kode dalam fungsi sebagai berikut:
```c
void writeLogMade(char createdDir[]){
    // menulis pesan log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logMADE[256] = "MADE ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMADE, createdDir);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logMADE);
    pthread_mutex_unlock(&lock);
}
```
Fungsi `writeLogMade` digunakan untuk menulis pesan log ke dalam file log.txt ketika direktori berhasil dibuat oleh program. Pesan log yang ditulis berisi informasi waktu pembuatan direktori dan nama direktori yang dibuat.

Berikut adalah penjelasan detail untuk setiap baris kode di dalam fungsi tersebut:

- `writeLogMade` 

    Menerima sebuah argumen bertipe char yang menyimpan nama direktori yang berhasil dibuat.

- Fungsi ini menggunakan fungsi `time` untuk mendapatkan informasi waktu sistem saat ini, dan menyimpan informasi tersebut ke dalam variabel `now`.

- Selanjutnya, informasi waktu tersebut diubah menjadi string format tanggal dan waktu dengan menggunakan fungsi `localtime` dan `strftime`, dan disimpan ke dalam variabel` _time_str`.

- Variabel `logMADE` didefinisikan sebagai string konstan "MADE ", yang akan digabungkan dengan nama direktori yang dibuat untuk membentuk pesan log.

- `pthread_mutex_lock(&lock)` 

    Mengunci penggunaan mutex sehingga tidak terjadi kesalahan pada saat menulis log.

- `fprintf` 

    Menulis pesan log ke dalam file **log.txt**. Pesan log terdiri dari informasi waktu yang disimpan dalam variabel `_time_str` dan informasi direktori yang dibuat yang disimpan dalam variabel `logMADE`.

- `pthread_mutex_unlock(&lock)`

    Membuka penggunaan mutex yang sebelumnya dikunci sehingga mutex bisa digunakan oleh thread lain.

Fungsi ini dipanggil setiap kali pembuatan folder dilakukan, yaitu setiap terdapat `mkdir`, maka fungsi ini akan dipanggil.

##### **5.2. log ACCESSED**
Untuk membuat log ACCESSED, kami membuat kode dalam fungsi sebagai berikut:

```c
void writeLogAccess(char folderPath[]){
    // menulis pesan log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logAccess[256] = "ACCESSED ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logAccess, folderPath);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logAccess);
    pthread_mutex_unlock(&lock);
}
```
Kode di atas adalah definisi dari fungsi `writeLogAccess` yang berfungsi untuk menulis pesan log ke file log. Fungsi ini menerima satu argumen yaitu `folderPath`, yang akan digunakan untuk menuliskan pesan log mengenai akses ke folder tertentu.

Pertama, fungsi ini mendapatkan waktu saat ini menggunakan fungsi `time()` dan `localtime()`, lalu menyimpannya ke dalam variabel `now` dan `t` berturut-turut. Selanjutnya, fungsi ini membuat string `logAccess` yang diawali dengan kata "ACCESSED " dan diikuti dengan `folderPath`.

Kemudian, fungsi ini mengunci mutex dengan menggunakan fungsi `pthread_mutex_lock()`, menuliskan pesan log menggunakan `fprintf()`, dan membuka kunci mutex dengan `pthread_mutex_unlock()`. Pesan log ditulis ke dalam file log dengan format waktu dan pesan log yang telah dibuat sebelumnya.

Fungsi ini dipanggil setiap kali suatu folder dibuka, yaitu saat terdapat perintah `fopen`. 

Selain itu, log ACCESSED juga dibuat saat pemindahan file dilakukan antar dua direktori yang berbeda. Pada kasus ini, maka akan dicatat path log ACCESSED untuk direktori path source dan juga path destination. Untuk melakukan hal ini, kami memanggil log ACCESSED juga ketika dengan log MOVED dilakukan. Pemindahan file dilakukan di dalam fungsi `create_directory` untuk memindahkan file ke categorized/[direktori extention], dan juga dilakukan pemindahan file di dalam main untuk memindahkan file yang masuk ke dalam kategori other ke categorized/other.

Berikut ini adalah kode **log ACCESSED untuk categorized/[direktori extention]**

```c
sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    system (logAccessSource);
                    system (logAccessTarget);
```

Kode tersebut berfungsi untuk menambahkan pesan log yang mencatat akses ke folder tertentu.

Pertama, kode menggunakan `sprintf` untuk membuat dua string pesan log baru, yaitu `logAccessSource` dan `logAccessTarget`. `logAccessSource` digunakan untuk mencatat akses ke folder yang sedang diproses saat ini, sedangkan `logAccessTarget` digunakan untuk mencatat akses ke folder tujuan atau hasil kategorisasi. Kedua string ini berisi command shell untuk menjalankan perintah `find` dan `echo`.

Pada `logAccessSource`, perintah `find` digunakan untuk mencari file dengan ekstensi tertentu (*.ext) di dalam direktori `hehe_dir` (direktori yang sedang diproses), dan outputnya akan diteruskan ke `xargs`, yang kemudian mengeksekusi perintah `sh` untuk mencetak pesan log yang berisi waktu akses dan direktori yang diakses.

Pada `logAccessTarget`, perintah `echo` digunakan untuk mencetak pesan log yang berisi waktu akses dan direktori tempat file-file hasil kategorisasi disimpan (`_dirName`).

Berikut ini adalah kode **log ACCESSED untuk categorized/other**

```c
 char logAccessSource[200];
 char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);

    system(logAccessSource);
    system(logAccessTarget);
```
Kode tersebut merupakan implementasi untuk mencatat aktivitas akses ke direktori atau file dalam sebuah log.

Pada kode pertama, `sprintf` digunakan untuk mengisi nilai ke dalam variabel `logAccessSource` dan `logAccessTarget` yang akan digunakan sebagai argumen untuk menjalankan perintah shell. `logAccessSource` digunakan untuk mencari file dalam direktori files dengan ekstensi tertentu, kemudian menampilkan aktivitas akses (tanggal, jam, dan nama direktori) ke dalam log. Sedangkan `logAccessTarget` digunakan untuk menambahkan pesan akses ke direktori lain (dengan nama direktori disimpan dalam variabel `otherPath`) ke dalam log.

Pada kode kedua, perintah shell `find` digunakan untuk mencari semua file dalam direktori files dan subdirektorinya, kemudian menampilkan aktivitas akses (tanggal, jam, dan nama direktori) ke dalam log menggunakan perintah `echo`. Perintah `>>` digunakan untuk menambahkan output ke file log tanpa menghapus isi yang sudah ada. Sedangkan pada `logAccessTarget`, perintah `echo` digunakan untuk menambahkan pesan akses ke direktori lain ke dalam log menggunakan format yang sama.

##### **5.3. log MOVED**
Untuk membuat log MOVED, kami melakukan **concatenation** setiap kali terdapat perintah bash `mv`. Fungsi `create_directory` dipakai untuk memindahkan file ke direktori categorized/[ekstensi file], sedangkan di dalam main dilakukan pemindahan file ke direktori categorized/other untuk file yang tidak masuk dalam kategori apapun. 

Berikut ini adalah kode log MOVED untuk **memindahkan file ke direktori categorized/[ekstensi file]**

```c
 sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);
```
Kode tersebut digunakan untuk memindahkan file dengan ekstensi tertentu ke dalam folder kategori yang sesuai, serta menambahkan pesan log "MOVED" ketika file dipindahkan.

`command3` digunakan untuk menentukan path tujuan pemindahan file dengan memindahkan file dengan ekstensi tertentu ke dalam folder dengan nama `_dirName`.

Sedangkan `command2` berfungsi untuk menambahkan pesan log "MOVED" ketika file dipindahkan, dengan mengeksekusi command `echo` dan menambahkan waktu saat pemindahan file dilakukan, nama file, ekstensi file dan direktori tujuan file dipindahkan ke dalam "log.txt". Setelah itu, `command3` dijalankan untuk memindahkan file ke direktori tujuan yang telah ditentukan sebelumnya.

Berikut ini adalah kode log MOVED untuk **memindahkan file ke direktori categorized/other**

```c
sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3);
```

Kode tersebut bertujuan untuk melakukan pemindahan file yang tidak memiliki ekstensi yang terdaftar ke dalam folder "other" dan mencatat setiap kali file dipindahkan dalam log.

Pada bagian pertama kode 

```c
sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
```

berfungsi untuk membuat perintah `mv` untuk memindahkan file dari path awal ke path tujuan yang ditentukan oleh `otherPath`. Dalam hal ini,` \\''{}'\\''` digunakan untuk memungkinkan pemindahan file dengan nama yang mengandung spasi atau karakter khusus lainnya.

Selanjutnya, pada kode di bawah ini:

```c
sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3); 
```

terdapat kode untuk mencatat pemindahan file ke dalam file log. Pada bagian ini, `%s` akan diisi dengan `otherPath` sehingga log akan mencatat path file tujuan, yaitu `categorized/other`. Log yang tercatat berupa pesan "MOVED other file : [nama file]" yang dilengkapi dengan waktu pemindahan. Kembali digunakan `\\''{}'\\''` untuk memungkinkan pencatatan file dengan nama yang mengandung spasi atau karakter khusus lainnya. Kemudian, `&& %s` digunakan untuk mengeksekusi perintah `mv` yang sudah dibuat sebelumnya.

#### 6. Membuat dan mengisi sub-direktori sesuai dengan extension yang terdapat pada file "extension.txt"
Di bawah ini adalah implemetasi kode yang kami buat untuk tahapan ini:

```c
// membaca nama direktori dari file, satu per satu
    while (fgets(dirname, MAX_LEN, fp)) {
        // menghilangkan karakter newline di akhir string
        dirname[strcspn(dirname, "\n")] = 0;

        // menghapus spasi di akhir string
        size_t len = strlen(dirname);
        while (len > 0 && isspace(dirname[len - 1])) {
            len--;
        }
        dirname[len] = '\0';

        // membuat direktori lain secara asynchronous dengan menggunakan thread
        strcpy(thread_args[i].dirname, dirname);
        thread_args[i].maxFile = maxFile;
        pthread_create(&tid[i], NULL, create_directory, &thread_args[i]);
        i++;
    }
```

Kode tersebut membaca nama direktori dari file "extensions.txt" dan membuat direktori baru secara **asynchronous** dengan menggunakan **thread**.

Pertama-tama, kode membuka file "extensions.txt" untuk dibaca. Kemudian, dengan menggunakan `while loop`, kode membaca nama direktori satu per satu dengan menggunakan fungsi `fgets()`. Fungsi ini membaca string dari file sebanyak ukuran yang ditentukan oleh parameter kedua, kemudian menyimpannya di dalam buffer yang ditentukan oleh parameter pertama.

Setelah nama direktori berhasil dibaca, kode menghilangkan karakter **newline (\n)** di akhir string menggunakan fungsi `strcspn()`. Kemudian, kode menghapus spasi di akhir string menggunakan fungsi `isspace()`.

Selanjutnya, kode membuat direktori lain secara asynchronous dengan menggunakan thread. Pertama-tama, nama direktori disalin ke dalam `thread_args[i].dirname` dan nilai maxFile disalin ke dalam `thread_args[i].maxFile`. Kemudian, fungsi `pthread_create()` digunakan untuk membuat thread baru. 

**Parameter pertama** berisi alamat dari variabel `tid[i]`, yaitu variabel yang akan digunakan untuk menyimpan id thread yang dibuat. **Parameter kedua** digunakan untuk mengatur attribute dari thread yang dibuat, dalam kasus ini diatur sebagai NULL. **Parameter ketiga** adalah fungsi yang akan dijalankan sebagai thread `create_directory`, dan parameter keempat adalah argumen untuk fungsi tersebut `&thread_args[i]`.

Berikut ini adalah kode untuk fungsi `create_directory` yang dibuat:
```c
void* create_directory(void* arg) {
    struct ThreadArgs* thread_args = (struct ThreadArgs*) arg;
    char* dirname = thread_args->dirname;
    int maxFile = thread_args->maxFile;
    char ext[4];
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/";
    char new_dirname[MAX_LEN];
    struct stat st;

    // membuat nama direktori baru dengan menambahkan prefix "categorized/"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);

    //hitung banyak file
    char countFile[256];
    char hehe_dir[20] ="files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    // Mengeksekusi command dan membuka stream untuk membaca outputnya
    FILE* fp;
    fp = popen(cmd, "r");
    if (fp == NULL) {
        printf("Error: Failed to execute command.\n");
    }

    // Membaca output dari stream dan menampilkannya ke layar
    writeLogAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    // ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);
    
    //menghitung total direktori yang dibuat per ext 
    int numDir;
    if (maxFile == 0){
        numDir = 1;
    } else {
        numDir = numFiles / maxFile ;
        if (numFiles % maxFile > 0)
            numDir = numDir + 1;
    }

    // Menutup stream dan mengembalikan nilai 0 (sukses)
    pclose(fp);

    //create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0) {
    // directory creation was successful, so do something
        writeLogMade(new_dirname);
    }

    for (int i = 0; i < numDir; i++)
    {
         // mendefinisikan variable nama directory dengan suffix sesuai dengan index
         char _dirName[30];
         strcpy(_dirName, new_dirname);
         if(i > 0) {
            char suffix[10];
            sprintf(suffix, " (%d)", i+1);
            strcat(_dirName, suffix);
        }       

        // memeriksa apakah direktori sudah ada
        if (stat(_dirName, &st) == 0) {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        } else {
                // membuat direktori jika belum ada
                if (mkdir(_dirName, 0777) == 0) {
                    writeLogMade(_dirName);
                    //move files according to their extensions
                    char command1[600];
                    char command2[500];
                    char command3[100];
                    char command4[600];
                    char logAccessSource[200];
                    char logAccessTarget[100];

                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    system (logAccessSource);
                    system (logAccessTarget);

                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

                    if (maxFile == 0)
                    {
                        sprintf(command4, "find %s/ -type f -iname \"*.%s\" |xargs -I {} sh -c '%s' ", hehe_dir, ext, command2);
                        system(command4);
                    } else {
                        sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2);

                        system(command1);

                    }


                } else {
                    perror("Gagal membuat direktori");
                    
                }
            }
    }
    return NULL;
}
```

Kode tersebut merupakan implementasi dari sebuah fungsi yang bertujuan untuk membuat direktori baru secara dinamis, berdasarkan jenis file ekstensi yang telah ditentukan sebelumnya.

Fungsi ini menerima sebuah argumen berupa struktur data `ThreadArgs` yang berisi informasi tentang nama direktori, jumlah file maksimum yang diperbolehkan dalam satu direktori, serta jumlah file dengan ekstensi yang sesuai dengan nama direktori tersebut.

Langkah-langkah yang dilakukan dalam fungsi ini adalah sebagai berikut:

##### **6.1. Membuat nama direktori baru dengan menambahkan prefix "categorized/" pada nama direktori asli.**

```c
    struct ThreadArgs* thread_args = (struct ThreadArgs*) arg;
    char* dirname = thread_args->dirname;
    int maxFile = thread_args->maxFile;
    char ext[4];
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/";
    char new_dirname[MAX_LEN];
    struct stat st;

    // membuat nama direktori baru dengan menambahkan prefix "categorized/"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);
```

Kode tersebut merupakan implementasi fungsi `create_directory` yang akan dijalankan oleh thread untuk membuat direktori baru. Fungsi ini menerima satu argumen arg berupa pointer ke struktur `ThreadArgs` yang berisi nama direktori dan nilai `maxFile`.

Pertama, argumen `arg` dikonversi menjadi pointer ke struktur `ThreadArgs` dengan menggunakan casting. Kemudian, nilai nama direktori dan maxFile disimpan dalam variabel `dirname` dan `maxFile`, masing-masing.

Selanjutnya, variabel `ext` dideklarasikan sebagai array karakter dengan ukuran 4 dan diisi dengan nilai `dirname`. Ini dilakukan untuk menyimpan ekstensi file yang akan dikategorikan.

Selanjutnya, variabel `categorized_dirname` dideklarasikan sebagai array karakter dengan ukuran `MAX_LEN` dan diinisialisasi dengan string "categorized/". Kemudian, variabel `new_dirname` dideklarasikan sebagai array karakter dengan ukuran `MAX_LEN`. Nilai `categorized_dirname` ditambahkan ke `new_dirname` menggunakan fungsi `strcpy` dan `strcat`, untuk membuat nama direktori baru dengan menambahkan prefix "categorized/" pada nilai dirname.


Akhirnya, struktur `st` dideklarasikan sebagai variabel untuk menyimpan informasi file/direktori dan fungsi `mkdir` digunakan untuk membuat direktori baru dengan nama `new_dirname`.


##### **6.2. Menghitung jumlah file dengan ekstensi yang sesuai dengan nama direktori tersebut.**

```c
    //hitung banyak file
    char countFile[256];
    char hehe_dir[20] ="files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    // Mengeksekusi command dan membuka stream untuk membaca outputnya
    FILE* fp;
    fp = popen(cmd, "r");
    if (fp == NULL) {
        printf("Error: Failed to execute command.\n");
    }

    // Membaca output dari stream dan menampilkannya ke layar
    writeLogAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    // ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);
```
Kode tersebut bertujuan untuk menghitung jumlah file dalam direktori "files" yang memiliki ekstensi tertentu. Secara rinci, berikut adalah penjelasan setiap baris kode:

- `char countFile[256];`

    Deklarasi array karakter untuk menampung output dari command `wc -l`.

- `char hehe_dir[20] ="files";`

    Mendeklarasikan variabel `hehe_dir` sebagai string "files".

- `char cmd[256];`
        
    Mendeklarasikan variabel cmd untuk menampung perintah `find` dan `wc -l`.
    
- `snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);`
   
    Mengisi variabel cmd dengan perintah `find` dan `wc -l` untuk menghitung jumlah file dalam direktori "files" yang memiliki ekstensi tertentu. `%s` diisi dengan variabel `hehe_dir` dan `%s` yang kedua diisi dengan variabel `ext`.
   
- `FILE* fp;`
   
    Mendeklarasikan variabel `fp` sebagai pointer ke file.
    
- `fp = popen(cmd, "r");`
     
    Mengeksekusi command yang disimpan dalam variabel `cmd` dan membuka stream untuk membaca outputnya.
    
- `if (fp == NULL) {...}`
      
    Mengecek apakah stream berhasil dibuka atau tidak.
    
- `writeLogAccess(hehe_dir);`
      
    Memanggil fungsi `writeLogAccess` untuk mencatat akses ke direktori "files" pada file log.
    
- `fgets(countFile, sizeof(countFile), fp);`
      
    Membaca output dari stream dan menyimpannya dalam variabel `countFile`.
    
- `int numFiles = atoi(countFile);`
       
    Mengkonversi string yang disimpan dalam variabel `countFile` menjadi integer dan menyimpannya dalam variabel `numFiles`.
    
- `char _write[MAX_LEN];`
       
    Mendeklarasikan variabel `_write` sebagai array karakter dengan ukuran `MAX_LEN`.
   
- `system("touch buffer.txt");`
       
    Menjalankan command touch buffer.txt untuk membuat file "buffer.txt" jika belum ada.
   
- `sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);`
       
    Mengisi variabel `_write` dengan command untuk menambahkan informasi jumlah file ke dalam file "buffer.txt". %s diisi dengan variabel `ext` dan `%d` diisi dengan variabel `numFiles`.
    
- `system(_write);`
      
    Menjalankan command yang disimpan dalam variabel `_write`.

##### **6.3. Menghitung jumlah direktori yang dibutuhkan untuk menyimpan seluruh file yang sesuai dengan ekstensi tersebut, berdasarkan batasan jumlah file maksimum dalam satu direktori yang telah ditentukan sebelumnya.**

```c
//menghitung total direktori yang dibuat per ext 
    int numDir;
    if (maxFile == 0){
        numDir = 1;
    } else {
        numDir = numFiles / maxFile ;
        if (numFiles % maxFile > 0)
            numDir = numDir + 1;
    }
// Menutup stream dan mengembalikan nilai 0 (sukses)
    pclose(fp);
```

Kode tersebut adalah untuk menghitung jumlah direktori yang dibuat berdasarkan jumlah file yang ada dalam satu ekstensi tertentu dan batasan maksimum file dalam satu direktori.

Pertama, variabel `numDir` dideklarasikan sebagai variabel yang akan menampung jumlah direktori yang dibuat.

Kemudian, dilakukan pengecekan apakah nilai `maxFile` (batasan maksimum file dalam satu direktori) sama dengan **0**. Jika iya, maka nilai `numDir` di-set menjadi **1** karena hanya akan dibuat satu direktori.

Jika `maxFile` tidak sama dengan 0, maka nilai `numFiles` (jumlah file dalam satu ekstensi tertentu) akan dibagi dengan `maxFile` untuk menghitung berapa banyak direktori yang dibutuhkan. Hasil pembagian ini akan disimpan ke dalam variabel `numDir`.

Selanjutnya, akan dilakukan pengecekan apakah terdapat sisa hasil bagi dari pembagian `numFiles` dengan `maxFile`. Jika ada, maka artinya ada file yang belum terakomodasi di direktori yang sudah dibuat. Oleh karena itu, nilai `numDir` akan **ditambah 1** untuk menampung direktori tambahan yang dibutuhkan.

Fungsi `pclose()` digunakan untuk menutup stream yang dibuka oleh fungsi `popen()`. Setelah stream ditutup, nilai pengembalian dari `pclose()` akan menjadi status akhir dari proses yang dijalankan menggunakan `popen()`. Dalam hal ini, nilai pengembalian 0 menandakan bahwa proses telah berhasil dijalankan dengan baik dan telah selesai. 

Dengan demikian, nilai `numDir` akan menunjukkan **jumlah total direktori** yang dibuat.

##### **6.4. Membuat direktori-direktori baru sesuai dengan jumlah yang telah dihitung sebelumnya.**

```c
//create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0) {
    // directory creation was successful, so do something
        writeLogMade(new_dirname);
    }
```
Kode tersebut merupakan implementasi dari fungsi untuk membuat direktori baru. Jika variabel `numDir` bernilai 0, maka itu berarti tidak perlu membuat subdirektori baru karena tidak ada file yang akan disimpan di dalamnya. Oleh karena itu, program akan membuat direktori baru dengan nama yang diberikan dalam parameter `new_dirname` menggunakan fungsi `mkdir()` yang merupakan fungsi bawaan dari sistem operasi linux.

Jika proses pembuatan direktori berhasil dilakukan (`mkdir()` mengembalikan nilai 0), maka fungsi `writeLogMade()` akan dipanggil dengan parameter nama direktori baru `(new_dirname)`. Fungsi ini bertugas untuk mencatat log atau riwayat pembuatan direktori dalam sebuah file log.txt yang telah ditentukan sebelumnya.

##### **6.5. Memindahkan file-file yang sesuai dengan ekstensi ke dalam direktori-direktori baru yang telah dibuat, dan menulis log berupa informasi tentang file yang telah dipindahkan, waktu pemindahan file, serta direktori tujuan pemindahan file.**

```c
for (int i = 0; i < numDir; i++)
    {
         // mendefinisikan variable nama directory dengan suffix sesuai dengan index
         char _dirName[30];
         strcpy(_dirName, new_dirname);
         if(i > 0) {
            char suffix[10];
            sprintf(suffix, " (%d)", i+1);
            strcat(_dirName, suffix);
        }       

        // memeriksa apakah direktori sudah ada
        if (stat(_dirName, &st) == 0) {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        } else {
                // membuat direktori jika belum ada
                if (mkdir(_dirName, 0777) == 0) {
                    writeLogMade(_dirName);
                    //move files according to their extensions
                    char command1[600];
                    char command2[500];
                    char command3[100];
                    char command4[600];
                    char logAccessSource[200];
                    char logAccessTarget[100];

                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    system (logAccessSource);
                    system (logAccessTarget);

                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

                    if (maxFile == 0)
                    {
                        sprintf(command4, "find %s/ -type f -iname \"*.%s\" |xargs -I {} sh -c '%s' ", hehe_dir, ext, command2);
                        system(command4);
                    } else {
                        sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2);

                        system(command1);

                    }


                } else {
                    perror("Gagal membuat direktori");
                    
                }
            }
    }
```

Kode ini merupakan bagian dari program untuk memindahkan file-file dengan ekstensi tertentu ke dalam beberapa direktori berdasarkan jumlah maksimal file dalam satu direktori yang ditentukan oleh pengguna. Kode ini akan menjalankan perulangan untuk membuat direktori sebanyak `numDir` kali.

Pertama-tama, kode akan mendefinisikan variable `_dirName` untuk menampung nama direktori dengan tambahan **suffix** (jika` numDir > 1`). Kemudian, kode akan memeriksa apakah direktori sudah ada menggunakan fungsi `stat`. Jika sudah ada, program akan mencetak pesan bahwa direktori sudah ada. Namun, jika belum ada, program akan membuat direktori tersebut menggunakan fungsi `mkdir` dan menulis pesan di file log menggunakan fungsi `writeLogMade`.

Setelah direktori berhasil dibuat, program akan memindahkan file-file dengan ekstensi tertentu dari direktori asal ke direktori tujuan dengan menggunakan perintah `mv`. Selain itu, program juga akan mencatat informasi akses ke direktori pada file **log.txt** menggunakan perintah `find` dan `xargs`.

Jika `maxFile == 0`, program akan memindahkan seluruh file dengan ekstensi tertentu ke direktori yang telah dibuat. Namun, jika `maxFile != 0`, program hanya akan memindahkan sejumlah file sesuai dengan jumlah maksimal file dalam satu direktori yang ditentukan oleh dalam file "max.txt". Jika file-file berhasil dipindahkan maka akan dicatat juga informasi log untuk MOVED ke dalam **log.txt** yang format syntaxnya disimpan dalam variabel `command2`.

Terakhir, jika pembuatan direktori gagal, program akan mencetak pesan kesalahan menggunakan fungsi `perror`.

#### 7. Menggabungkan thread yang masih berjalan
Berikut ini adalah kode yang kami buat untuk tahap ini:
```c
// join thread yang masih berjalan
    for (int j = 0; j < i; j++) {
        pthread_join(tid[j], NULL);
    }
```

Kode di atas merupakan loop untuk menjalankan fungsi `pthread_join` yang digunakan untuk menggabungkan thread yang masih berjalan ke dalam thread utama.

Loop for di atas akan dijalankan sebanyak `i` kali, dengan `i` merupakan jumlah thread yang telah dibuat. Dalam setiap iterasinya, fungsi `pthread_join` akan dipanggil untuk menggabungkan thread dengan `id tid[j]` ke dalam thread utama.

Fungsi `pthread_join` memungkinkan program untuk menunggu hingga thread yang dimaksud selesai berjalan sebelum program melanjutkan ke baris kode selanjutnya. Hal ini memastikan bahwa semua thread telah selesai dieksekusi sebelum program berakhir. Parameter kedua dari `pthread_join` `(NULL) `tidak digunakan dalam kode ini dan digunakan untuk mengembalikan nilai pengembalian dari thread yang bergabung ke dalam thread utama.

#### 8. Membuat sub-direktori "other" di dalam direktori "categorized"
Untuk bagian sub-direktori other, dilakukan di dalam **main**, karena hanya dapat dilakukan setelah semua file disortir berdasarkan extension yang tersedia. 

Untuk melakukan tahapan ini, kami membuat kode sebagai berikut:

```c
// membuat direktori "other" di dalam "categorized" jika belum ada
    if (stat("categorized/other", &st) != 0) {
        writeLogMade("categorized/other");
        if (mkdir("categorized/other", 0777) != 0)
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n");
    }
```

Kode ini berfungsi untuk membuat direktori "other" di dalam direktori "categorized" jika direktori tersebut belum ada.

Pertama-tama, kode ini melakukan pemeriksaan terhadap direktori "categorized/other" menggunakan fungsi `stat()`. Jika direktori tersebut tidak ada, maka akan dipanggil fungsi `mkdir()` untuk membuat direktori "other" di dalam direktori "categorized".

Selain itu, kode ini juga memanggil fungsi `writeLogMade()` untuk mencatat pesan log bahwa direktori "other" baru saja dibuat. Fungsi `writeLogMade()` akan menuliskan pesan log dengan format "MADE [nama direktori]" pada file log yang telah ditentukan sebelumnya.

#### 9. Mengisi sub-direktori "other" untuk file dengan ekstensi selain yang tercantum pada file "extension.txt" dan menuliskan pesan log ke dalam file log.txt.
Berikut ini adalah kode yang kami buat untuk tahap ini:

```c
// move other files to categorized/other
    char cmdOther1[600];
    char cmdOther2[500];
    char cmdOther3[100];
    char otherPath[20] = "categorized/other";
    char logAccessSource[200];
    char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);
    
    system(logAccessSource);
    system(logAccessTarget);

    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3);
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2);
    system(cmdOther1);
```
Kode tersebut merupakan bagian dari program untuk mengkategorikan file-file yang ada pada direktori "files" berdasarkan ekstensi file tersebut. Kode tersebut bertanggung jawab untuk memindahkan file yang tidak memiliki ekstensi yang terdaftar ke dalam direktori "categorized/other".

Pertama, program akan melakukan pengecekan apakah direktori "categorized/other" sudah ada atau belum menggunakan fungsi `stat`. Jika direktori belum ada, maka program akan membuat direktori tersebut dan mencatat pembuatan direktori pada log dengan memanggil fungsi `writeLogMade`.

Setelah itu, program akan mengeksekusi perintah untuk memindahkan file-file pada direktori "files" yang tidak memiliki ekstensi yang terdaftar ke dalam direktori "categorized/other". Proses ini dilakukan dengan membangun beberapa string perintah menggunakan `sprintf`, yaitu `cmdOther3`, `cmdOther2`, dan `cmdOther1`.

`cmdOther3` adalah string perintah untuk memindahkan file yang tidak terdaftar ekstensinya ke dalam direktori "categorized/other". `cmdOther2` adalah string perintah untuk mencatat perpindahan file pada log, yang mencakup waktu perpindahan dan nama file yang dipindahkan. Sedangkan `cmdOther1` adalah string perintah utama yang akan mengeksekusi `cmdOther2` untuk setiap file yang ditemukan pada direktori "files". Kemudian, program mengeksekusi `cmdOther1` menggunakan fungsi `system`.

Sebelum dan sesudah proses pemindahan, program juga akan mencatat akses ke direktori "files" dan "categorized/other" pada log dengan memanggil fungsi `system` untuk mengeksekusi perintah yang disimpan dalam string `logAccessSource` dan `logAccessTarget`.

#### 10. Mengeluarkan pada terminal banyak file tiap extension secara ascending beserta banyak file sub-direktori other
Untuk menghitung banyak file pada setiap ekstensi secara ascending, kami membaginya menjadi **dua bagian**. Bagian pertama adalah untuk menghitung jumlah file pada setiap ekstensi, sedangkan bagian kedua adalah untuk menghitung jumlah file di sub-direktori "other". Hal ini dilakukan karena penghitungan jumlah file di sub-direktori "other" harus dilakukan setelah berhasil menghitung jumlah file di setiap ekstensi. Oleh karena itu, penghitungan jumlah file di sub-direktori "other" ditempatkan pada `main`, sedangkan penghitungan jumlah file di setiap ekstensi ditempatkan di dalam fungsi `create_directory`.

Pada bagian menghitung banyak file tiap extension dalam tiap extension, diperlukan file buffer.txt, yang kami buat sebagai tempat untuk menyimpan isi dari jumlah file setiap extension. Pada fungsi `create_directory` dilakukan dengan **multithread**, sehingga kita tidak bisa mengatur untuk mengeluarkan output secara ascending terlebih dahulu. Maka dari itu, setiap kali perhitungan jumlah file dilakukan, dicatat di dalam buffer.txt terlebih dahulu.

Berikut ini adalah kode yang untuk menghitung banyak file tiap extension dan mencatatnya ke "buffer.txt". Kode ini terletak pada bagian awal fungsi, yaitu pada saat ingin menghitung banyaknya direktori yang harus dibuat tiap extension. 

```c
// ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);
```
Kode tersebut digunakan untuk menambahkan sebuah baris teks pada file "buffer.txt" dengan format "extension_[ext] : [jumlah file]". Variabel `ext` dan `numFiles` masing-masing berisi ekstensi file dan jumlah file yang ditemukan dengan ekstensi tersebut.

Pertama-tama, digunakan perintah `touch` untuk membuat file "buffer.txt" jika belum ada. Kemudian, teks yang ingin ditambahkan pada file tersebut disusun dengan menggunakan fungsi `sprintf()` dan disimpan pada variabel `_write`. Teks tersebut akan menampilkan informasi jumlah file yang ditemukan pada direktori yang sesuai dengan ekstensi file.

Selanjutnya, teks yang disimpan pada variabel `_write` akan dituliskan ke dalam file "buffer.txt" dengan menggunakan perintah `echo` dan tanda `>>` yang menunjukkan bahwa teks tersebut akan ditambahkan pada akhir file. Setelah itu, perintah `sort` digunakan untuk mengurutkan isi file "buffer.txt" secara ascending (menaik), dan perintah `rm -rf` digunakan untuk menghapus file "buffer.txt".

Setelah, semua extension file sudah dihitung jumlahnya ke dalam file "buffer.txt", kita barus bisa mengurutkannya secara ascending untuk dikeluarkan ke terminal. Pada bagian kode ini, dilakukan pada `main` sebagai berikut:

```c
//output sort buffer.txt
    system("sort buffer.txt");
    system("rm -rf buffer.txt");
```

Kode ini menjalankan dua perintah pada terminal, yaitu `sort` dan `rm`. Pertama, perintah `sort` digunakan untuk mengurutkan isi file "buffer.txt". Setelah itu, perintah `rm` digunakan untuk menghapus file "buffer.txt". Sehingga, kode ini akan mengurutkan isi file "buffer.txt" dan kemudian menghapus file tersebut.

Dari kode tersebut, akan dihasilkan output yang menampilkan jumlah file untuk setiap ekstensi sesuai dengan format yang diminta dan disusun secara ascending. 

Langkah terakhir adalah menghitung jumlah file dalam subfolder categorized/other. Sebelum melakukannya, kita perlu **menghitung berapa banyak file** yang ada di dalam subfolder tersebut. Berikut adalah kode yang digunakan untuk melakukan penghitungan tersebut:

```c
// count how many files in dir categorized/other
    DIR *dir;
    struct dirent *ent;
    int count = 0;
    if ((dir = opendir (otherPath)) != NULL) {
        writeLogAccess(otherPath);
        while ((ent = readdir (dir)) != NULL) {
            if(ent->d_type == DT_REG) {
                count++;
            }
        }
        closedir (dir);
    } else {
        perror ("Tidak bisa membuka direktori");
        return 1;
    }
    printf("other : %d\n", count);
```

Kode ini bertujuan untuk menghitung jumlah file yang terdapat di dalam direktori "categorized/other". 

Pertama-tama, dilakukan pembukaan direktori `otherPath` menggunakan fungsi `opendir()`. Jika berhasil membuka direktori, maka fungsi `readdir()` akan dipanggil untuk membaca setiap file yang terdapat di dalam direktori. Setiap file yang terbaca akan dicek tipe datanya apakah regular file atau bukan menggunakan `ent->d_type`. Jika tipe datanya adalah regular file, maka variabel `count` akan di-increment. Setelah semua file dibaca, direktori akan ditutup menggunakan fungsi `closedir()`. Jika gagal membuka direktori, maka akan ditampilkan pesan error. Jumlah file yang terhitung akan ditampilkan pada output menggunakan `printf()`.

### Soal F 
**Solusi :**
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    // Accessed count
    printf("\e[33mTotal banyak akses:\e[0m\n");
    system("grep ACCESSED log.txt | wc -l");

    // List folders & total file
    printf("\e[33m\nList folder & total file tiap folder:\e[0m\n");
    system("grep 'MOVED\\|MADE' log.txt \
    | awk -F 'categorized' '{print \"categorized\"$NF}' \
    | sed '/^$/d' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'");

    // total file tiap extension terurut secara ascending
    printf("\n\e[33mTotal file tiap extension\e[0m\n");
    system("grep 'MADE\\|MOVED' log.txt \
    | grep -o 'categorized/[^o]*' \
    | sed 's/categorized\\///' \
    | sed '/^$/d' \
    | sed 's/ \\([^o]*\\)//' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \" \"=\" \" \" $1-1}'");

    return 0;
}
```
Dalam pengimplementasi permintaan soal, berikut adalah tahapan yang kami implementasi pada program:
1. Menghitung banyak akses
2. Membuat list folder dan total file di direktori categorized/
3. Membuat total file tiap extension secara ascending

#### 1. Menghitung banyak akses
Format log untuk akses adalah sebagai berikut:

**DD-MM-YYYY HH:MM:SS ACCESSED [folder path]**

Maka kita dapat langsung mengambil kata kunci "ACCESSED" dari file log.txt dan menghitungnya, sebagai berikut:

```c
// Accessed count
    printf ("\e[33mTotal banyak akses:\e[0m\n");
    system("grep ACCESSED log.txt | wc -l");
```

Kode ini bertujuan untuk menghitung total jumlah baris dalam file log.txt yang mengandung string "ACCESSED". 

Pada bagian pertama, digunakan perintah `printf` untuk menampilkan teks "Total banyak akses:" dengan warna **kuning** (ditandai dengan kode `\e[33m dan \e[0m`). 

Kemudian pada bagian kedua, digunakan perintah `system` untuk mengeksekusi perintah shell `grep ACCESSED log.txt | wc -l`. Perintah `grep` akan mencari semua baris dalam file log.txt yang mengandung string "ACCESSED", dan perintah `wc` akan menghitung jumlah baris yang ditemukan. Hasil dari perintah tersebut akan ditampilkan pada terminal sebagai output dari program.


#### 2. Membuat list folder dan total file di direktori yang terbentuk
Pada bagian ini, untuk mendapatkan list seluruh nama folder yang dibuat maka kita perlu mengambil dari data kunci "MADE". 

Untuk menghitung total file di setiap direktori categorized/, kita dapat menggunakan log dari "MOVED" dengan memperhatikan bagian akhir dari log yang menunjuk kepada direktori destination. 

Agar kedua log MADE dan MOVED terpilih untuk menghitung jumlah tiap file di direktori categorized/ kita dapat memilih kata kunci "categorized" agar mengetahui semua sub-direktori yang terdapat di dalam categorized/. Berikut ini adalah implementasi kodenya:

```c
// List folders & total file
    printf ("\e[33m\nList folder & total file tiap folder:\e[0m\n");
    system("grep 'MOVED\\|MADE' log.txt \
    | awk -F 'categorized' '{print \"categorized\"$NF}' \
    | sed '/^$/d' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'");
```

Kode tersebut digunakan untuk menampilkan list folder dan jumlah total file pada tiap folder setelah proses pemindahan file selesai.

Berikut adalah penjelasan detail dari setiap perintah yang ada pada kode:

- `printf ("\e[33m\nList folder & total file tiap folder:\e[0m\n")`

    Digunakan untuk menampilkan pesan "List folder & total file tiap folder" dengan format teks berwarna kuning (33m) pada terminal.

- `system("grep 'MOVED\|MADE' log.txt")`

    Digunakan untuk mencari dan menampilkan baris yang mengandung kata "MOVED" atau "MADE" pada file log.txt.

- `awk -F 'categorized' '{print "categorized"$NF}'`

    Digunakan untuk memisahkan baris yang berisi path file yang sudah dipindahkan ke dalam folder "categorized" dan mengambil path tersebut sebagai output.

- `sed '/^$/d'`

    Digunakan untuk menghapus baris yang hanya berisi karakter new line.

- `sort`

    Digunakan untuk mengurutkan output secara ascending.

- `uniq -c`

    Digunakan untuk menghitung jumlah kemunculan dari tiap baris.

- `awk '{print $2 " "$3 "=" " " $1-1}'`

    Digunakan untuk memformat output dengan menampilkan path folder (dari kolom kedua), jumlah file pada folder tersebut (dari kolom ketiga), dan tanda sama dengan (=) di antara keduanya. Jumlah file yang ditampilkan dikurangi 1 untuk menghilangi saat log MADE subfoler dibuat, karena yang diminta hanyalah jumlah file di setiap direktori.

#### 3. Membuat total file tiap extension secara ascending
Pada tahap ini, langkah yang dilakukan mirip dengan langkah ke 2, karena kita memerlukan semua nama extension yang ada maka perlu mengambil dari log MADE dan MOVED. Kemudian untuk memperoleh extensionnya saja, kita dapat mengambil dari kata kunci setelah "categorized/". Selanjutnya, untuk menghilangkan direktori dengan suffix numbering kita dapat menggunakan fungsi `sed`. Berikut ini adalah implementasi kodenya:

```c
// total file tiap extension terurut secara ascending
    printf ("\n\e[33mTotal file tiap extension\e[0m\n");
    system("grep 'MADE\\|MOVED' log.txt \
    | grep -o 'categorized/[^o]*' \
    | sed 's/categorized\\///' \
    | sed '/^$/d' \
    | sed 's/ \\([^o]*\\)//' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \" \"=\" \" \" $1-1}'");
```
- `printf ("\n\e[33mTotal file tiap extension\e[0m\n");`

    Digunakan untuk mencetak pesan "Total file tiap extension" pada terminal.

- `system("grep 'MADE\\|MOVED' log.txt \ ... `

    Digunakan untuk melakukan pencarian pada file log.txt untuk mencari baris yang mengandung kata "MADE" atau "MOVED". Kemudian output hasil pencarian tersebut akan di-pipe (|) ke perintah selanjutnya.

- `grep -o 'categorized/[^o]*' \ ... `

    Digunakan untuk melakukan pencarian pada output sebelumnya untuk mencari pola string "categorized/[^o]*" yang artinya mencari string yang diawali dengan "categorized/" dan diikuti dengan karakter selain "o". Opsi "-o" digunakan untuk hanya mengeluarkan hasil pencarian berupa string yang sesuai dengan pola yang dicari.

- `sed 's/categorized\\///' \ ... `

    Digunakan untuk menghilangkan string "categorized/" pada output sebelumnya.

- `sed '/^$/d' \ ... `
    
    Digunakan untuk menghilangkan baris yang kosong pada output sebelumnya.

- `sed 's/ \\([^o]*\\)//' \ ... `
    
    Digunakan untuk menghilangkan string yang mengandung karakter "o" pada output sebelumnya.

- `sort \ ...` 
    
    Digunakan untuk mengurutkan hasil output sebelumnya.

- `uniq -c \ ...` 
    
    Digunakan untuk menghitung jumlah kemunculan setiap string pada output sebelumnya.

- `awk '{print $2 \" \" \"=\" \" \" $1-1}'"); `

    Digunakan untuk mencetak hasil output sebelumnya dengan format `nama ekstensi = jumlah file - 1`. Hal ini dikarenakan perhitungan ekstensi akan bertambah 1 ketika grep log MADE dilakukan, yang dimana bukan merupakan bagian dari total file sehingga harus dikurangi 1.

**Screenshot Output Program Nomor 4 :**
| <p align="center"> Screenshot </p> | <p align="center"> Deskripsi </p> |
| ---------------------------------- | ----------------------------------- |
|<img src="/uploads/215506b2ecd1b6f1a7d04c7f82ab194e/no4.png" width = "350"/> | <p align="center">download dan unzip</p> |
|<img src="/uploads/a9371b80f8a57a67ead98a138c6e1512/no4b.png" width = "350"/> | <p align="center">files folder before categorized</p> |
|<img src="/uploads/a32a95000bdc5d6efa072b4b908d2ef8/no4c.png" width = "350"/> | <p align="center">files folder after categorized, as well as categorized folder</p> |
|<img src="/uploads/f11fa1177b579314aa1b3469d74e3d31/no4d.png" width = "350"/> | <p align="center">log checker</p> |
|<img src="/uploads/af25996b5cb2e6f8d48b11cf1308ad28/no4logpng.png" width = "350"/> | <p align="center">log.txt contents</p> |

