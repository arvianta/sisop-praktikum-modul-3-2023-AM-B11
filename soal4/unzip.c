// Rayhan Arvianta Bayuputra - 5025211217

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <dirent.h>

int main()
{
    pid_t pid;
    int status;
    pid = fork();

    if (pid < 0) {
        perror("fork");
    }
    if(pid == 0) {
        execlp("wget", "wget", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL);
        exit(0);
    } else {
        waitpid(pid, &status, 0);
        pid = fork();
        
        if (pid < 0) {
            perror("fork");
        }
        if (pid == 0) {
            execlp("unzip", "unzip", "hehe.zip", NULL);
        }
    }
    return 0;
}