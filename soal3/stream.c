#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <unistd.h>

const char *base64_table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

//decode from base64 to string.
char *base64Decoder(const char *input){
    size_t input_len = strlen(input); //count the length of input (string)
    char *output = malloc(((input_len * 3) / 4) + 1); //allocate memory for the output

    //check if memory allocation was successful
    if (output == NULL) {
        return NULL;
    }

    int i = 0, j = 0;

    //loop through the inputstring
    while (i < input_len) {
        //extract each characters from base64
        int sextet_a = input[i] == '=' ? 0 : 
            strchr(base64_table, input[i]) - base64_table;
        int sextet_b = input[i + 1] == '=' ? 0 : 
            strchr(base64_table, input[i + 1]) - base64_table;
        int sextet_c = input[i + 2] == '=' ? 0 : 
            strchr(base64_table, input[i + 2]) - base64_table;
        int sextet_d = input[i + 3] == '=' ? 0 : 
            strchr(base64_table, input[i + 3]) - base64_table;

        //combine sextet_a, sextet_b, sextet_c, sextet_d into a single 24 bit
        int triple = (sextet_a << 18) + (sextet_b << 12) + (sextet_c << 6) + sextet_d;

        //extract each byte from the 24 bit
        output[j++] = (triple >> 16) & 0xFF;

        if (input[i + 2] != '=') {
            output[j++] = (triple >> 8) & 0xFF;
        }

        if (input[i + 3] != '=') {
            output[j++] = triple & 0xFF;
        }

        i += 4;
    }

    output[j] = '\0'; //add null to the end of output
    return output;
}

//decrypt text
char *rot13Decoder(const char *str){
    char *result = malloc(strlen(str) + 1); //allocate the decrypted string

    if (result == NULL) {
        return NULL;
    }

    int i;
    for (i = 0; str[i] != '\0'; i++) {
        if (str[i] >= 'a' && str[i] <= 'm') {
            result[i] = str[i] + 13;
        } else if (str[i] >= 'n' && str[i] <= 'z') {
            result[i] = str[i] - 13;
        } else if (str[i] >= 'A' && str[i] <= 'M') {
            result[i] = str[i] + 13;
        } else if (str[i] >= 'N' && str[i] <= 'Z') {
            result[i] = str[i] - 13;
        } else {
            result[i] = str[i]; //if the characters are not alphabet
        }
    }

    result[i] = '\0';
    return result;
}

//change hexadecimal string into ASCII
char *hexDecoder(const char *input) {
    size_t input_len = strlen(input); //count the length of input

    if (input_len % 2 != 0) { //input_len must genap
        return NULL;
    }

    //allocate memory
    char *output = malloc(input_len / 2 + 1);

    if (output == NULL) { //failed to allocate memory
        return NULL;
    }

    //loop through the input by 2 bytes
    for (int i = 0, j = 0; i < input_len; i += 2, j++) {
        char byte_str[3] = {input[i], input[i + 1], '\0'}; //store 2 bytes
        output[j] = (char)strtol(byte_str, NULL, 16); //convert the byte string into a char
    }

    output[input_len / 2] = '\0';
    return output;
}

void cmd(char *command[]) {
    pid_t pid = fork(); 

    if (pid < 0) { //pid error
        printf("ERROR: fork() failed\n");
        exit(EXIT_FAILURE);
    } else if (pid == 0) { //child process
        //error if execvp returns -1
        if(execvp(command[0], command) == -1) {
            exit(EXIT_FAILURE);
        }
    } else { //pid > 0 = parent process
        int status;
        wait(&status); //wait for the child process
    } 
}

//convert to uppercase
void toUpperCase(char *str) {
    while (*str) {
        *str = toupper(*str);
        str++;
    }
}

void decryptPlaylist() {
    FILE *f = fopen("song-playlist.json", "r"); //open song playlist

    //download from URL
    if (f == NULL) {
        char *url = "https://transfer.sh/get/tN3N7I/song-playlist.json";
        cmd((char *[]){"wget", "-O", "song-playlist.json", "-q", url, NULL});
    }

    //array to hold the file contents
    char fileContent[4096][256] = {""};
    char line[256];
    int i = 0;

    //copy file contents into fileContent[]
    while (fgets(line, sizeof(line), f)) {
        if (strlen(line) > 6) {
            line[strcspn(line, "\n")] = '\0';
            strcpy(fileContent[i++], line);
        }
    }

    //new file for the decrypted playlist
    FILE *out = fopen("playlist.txt", "w");

    if (out == NULL) {
        printf("ERROR: could not create playlist.txt\n");
        exit(1);
    }

    //decoding and writing each song title
    for (int j = 0; j < i; j += 2) {
        //extract the encryption for the current song title
        char *encryptMethod = fileContent[j];
        encryptMethod += 14;
        encryptMethod[strlen(encryptMethod) - 3] = '\0';

        //extract the encoded song title
        char *songTitle = fileContent[j + 1];
        songTitle += 12;
        songTitle[strlen(songTitle) - 2] = '\0';

        //decode the song title
        char *decodedString;
        if (strcmp(encryptMethod, "rot13") == 0) {
            decodedString = rot13Decoder(songTitle);
        } else if (strcmp(encryptMethod, "base64") == 0) {
            decodedString = base64Decoder(songTitle);
        } else if (strcmp(encryptMethod, "hex") == 0) {
            decodedString = hexDecoder(songTitle);
        }

        fprintf(out, "%s\n", decodedString);
    }

    //sort the output alphabetically
    cmd((char *[]){"sort", "playlist.txt", "-o", "playlist.txt", NULL});
}

//search for songs
void playSong(char playlist[1024][128], const char *keyword) {
    int songCount = 0;
    char searchResult[1024][128] = {""};

    //loop through the playlist
    for (int i = 0; i < 1024; i++) {
        //if the playlist is not empty
        if (strcmp(playlist[i], "") != 0) {
            //convert the playlist to uppercase
            char *line = playlist[i];
            toUpperCase(line);
            
            //add the found keyword to searchResult[]
            if (strstr(line, keyword) != NULL) {
                strcpy(searchResult[songCount++], line);
            }
        }
    }

    if (songCount == 0) { //no songs found
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", keyword);
    } else if (songCount == 1) { //only 1 song found
        printf("USER <USER_ID> PLAYING \"%s\"\n", searchResult[0]);
    } else { //more than 1 song found
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\":\n", songCount, keyword);
        
        for (int i = 0; i < songCount; i++) {
            printf("%d. %s\n", i + 1, searchResult[i]);
        }
    }
}

typedef struct Message {
    long msg_type;
    char msg_text[128];
} Message;

int main() {
    char command[128];
    char playlist[1024][128] = {""}, line[128];
    int songCount = 0;

    //open the plalist files
    FILE *f = fopen("playlist.txt", "r");

    if (f != NULL) {
        while (fgets(line, sizeof(line), f)) {
            line[strcspn(line, "\n")] = '\0'; // remove newline
            strcpy(playlist[songCount++], line);
        }
    }

    //create message queue
    key_t key = ftok("msgqueue", 65);
    int msg_id = msgget(key, 0666 | IPC_CREAT);

    while (1) {
        //receive message
        Message msg;
        msgrcv(msg_id, &msg, sizeof(msg), 1, 0);

        char *command = msg.msg_text; //extract the command

        //check the command
        if (strcmp(command, "DECRYPT") == 0) {
            decryptPlaylist();
        } else if (strcmp(command, "LIST") == 0) {
            //print the paylist
            for (int i = 0; i < songCount; i++) {
                printf("%s\n", playlist[i]);
            }
        } else if (strstr(command, "PLAY") != NULL) {
            char *keyword = command;
            keyword += 5; //skip "PLAY"
            toUpperCase(keyword); //convert to uppercase
            playSong(playlist, keyword); //play the song
        } else if (strstr(command, "ADD") != NULL) {
            //extract the song and add it to the playlist
            char *title = command;
            title += 4; //skip "ADD"
            strcpy(playlist[songCount++], title); //copy the song to the playlist
            printf("USER <ID_USER> ADD %s\n", title);
        } else {
            printf("UNKNOWN COMMAND\n");
        }
    }
  return 0;
}