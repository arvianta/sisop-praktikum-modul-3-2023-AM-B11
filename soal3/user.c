#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>

typedef struct Message {
	long msg_type;
	char msg_text[128];
} Message;

int main() {
	key_t key = ftok("msgqueue", 65); //create key for the msgqueue
	int msg_id = msgget(key, 0666 | IPC_CREAT); //crate msgqueue with the key
	
	Message msg; //create message
  	msg.msg_type = 1; //set to 1
	
	//read commands and send as messages
  	while (1) {
    	printf("COMMAND: ");
    	scanf(" %[^\n]", msg.msg_text);
		
		//send the message to the message queue
    	msgsnd(msg_id, &msg, sizeof(msg), 0);
  	}

  	return 0;
}
