// Rayhan Arvianta Bayuputra - 5025211217

#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

int main() {
    
    key_t key = 2104;
    int (*result)[4][5];

    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
    result = shmat(shmid, NULL, 0);

    int matrix1[4][2];
    int matrix2[2][5];
    int i, j, k;

    // Generate random numbers for matrix 1
    srand(time(NULL));
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 2; j++) {
            matrix1[i][j] = rand() % 5 + 1;
        }
    }

    // Generate random numbers for matrix 2
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 5; j++) {
            matrix2[i][j] = rand() % 4 + 1;
        }
    }

    // Initialize result matrix with 0
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            (*result)[i][j] = 0;
        }
    }

    // Multiply matrix 1 and matrix 2
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            for (k = 0; k < 2; k++) {
                (*result)[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }

    // Display result matrix
    printf("Result matrix:\n");
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("[%d] ", (*result)[i][j]);
        }
        printf("\n");
    }

    return 0;
}