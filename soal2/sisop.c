// Rayhan Arvianta Bayuputra - 5025211217

#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

#define ull unsigned long long

int (*result)[4][5];
ull result_factorial[4][5];

int main()
{
    clock_t start, end;
    double cpu_time_used;

    start = clock();

    key_t key = 2104;
    int shmid = shmget(key, sizeof(int[4][5]), 0666);
    result = shmat(shmid, NULL, 0);

    int i, j, k;
    printf("\nResult matrix:\n");
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            printf("[%d] ", (*result)[i][j]);
        }
        printf("\n");
    }

    printf("\nHasil faktorial matrix:\n");
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            ull factorial = 1;
            for (k = 1; k <= (*result)[i][j]; k++)
            {
                factorial *= k;
            }
            result_factorial[i][j] = factorial;
        }
    }

    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            printf("[%llu] ", result_factorial[i][j]);
        }
        printf("\n");
    }

    end = clock();
    cpu_time_used = ((double)(end - start)) * 1000 / CLOCKS_PER_SEC;
    printf("\nCPU elapsed time when running: %f ms\n", cpu_time_used);

    return 0;
}