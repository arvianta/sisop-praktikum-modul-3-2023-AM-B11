#include<stdatomic.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>

typedef struct HuffmanNode {
	int frequency;
	char character;
	struct HuffmanNode *left, *right;
} HuffmanNode;

HuffmanNode *createHuffmanNode() {
	HuffmanNode *h = malloc(sizeof(*h));
	h->character = 0;
	h->frequency = 0;
	h->left = NULL;
	h->right = NULL;
	return h;
}

HuffmanNode *createParent(HuffmanNode *left, HuffmanNode *right) {
	HuffmanNode *h = malloc(sizeof(*h));
	h->character = 0;
	h->frequency = left->frequency + right->frequency;
	h->left = left;
	h->right = right;
	return h;
}

typedef struct PQueueNode {
	HuffmanNode *data;
	struct PQueueNode *next;
} PQueueNode;

typedef struct PQueue {
	PQueueNode *top;
	int size;
} PQueue;

PQueueNode *createPQNode(HuffmanNode *data) {
	PQueueNode *newNode = malloc(sizeof(*newNode));
	newNode->data = data;
	newNode->next = NULL;
	return newNode;
}

PQueue *createPQueue() {
	PQueue *pq = malloc(sizeof(*pq));
	pq->top = NULL;
	pq->size = 0;
	return pq;
}

int pqIsEmpty(PQueue *pq) {
	return (pq->top == NULL);
}

void pqPush(PQueue *pq, HuffmanNode *data) {
	PQueueNode *temp = pq->top;
	PQueueNode *newNode = createPQNode(data);
	pq->size++;

	if (pqIsEmpty(pq)) {
		pq->top = newNode;
		return;
	}

  	if(data->frequency < pq->top->data->frequency) {
    	newNode->next = pq->top;
    	pq->top = newNode;
  	} else {
    
	while(temp->next != NULL && temp->next->data->frequency < data->frequency) {
      	temp = temp->next;
    }

	newNode->next = temp->next;
	temp->next = newNode;
	}
}

void pqPop(PQueue *pq) {
  	if (!pqIsEmpty(pq)) {
    	PQueueNode *temp = pq->top;
    	pq->top = pq->top->next;
    	pq->size--;
    	free(temp);
  }
}

HuffmanNode *pqTop(PQueue *pq) {
  	if(!pqIsEmpty(pq)) {
    	return pq->top->data;
  	}
  	return NULL;
}

HuffmanNode *createHuffmanTree(int charFrequency[]) {
  	PQueue *pq = createPQueue();

  	for(int i = 0; i < 26; i++) {
    	if (charFrequency[i] > 0) {
      		HuffmanNode *h = createHuffmanNode();
      		h->character = (char)(i + 'A');
      		h->frequency = charFrequency[i];
      		pqPush(pq, h);
    	}
  	}

  	while (pq->size > 1) {
    	HuffmanNode *left = pq->top->data;
    	pqPop(pq);

	    HuffmanNode *right = pq->top->data;
	    pqPop(pq);
	
	    HuffmanNode *parent = createParent(left, right);
	    pqPush(pq, parent);
  	}

  	return pq->top->data;
}

void traverseHuffmanTree(HuffmanNode *root, char *code, char huffmanMap[][16]) {
  	if (root->left != NULL) {
		char new_code[128];
		strcpy(new_code, code);
		traverseHuffmanTree(root->left, strcat(new_code, "0"), huffmanMap);
	
	    strcpy(new_code, code);
	    traverseHuffmanTree(root->right, strcat(new_code, "1"), huffmanMap);
  	} else {
	    strcpy(huffmanMap[(int)(root->character - 'A')], code);
  	}
}


void cmd(char *command[]) {
  	pid_t pid = fork();

  	if(pid < 0){
	    printf("ERROR: fork() failed\n");
	    exit(EXIT_FAILURE);
  	} else if(pid == 0){
    	if(execvp(command[0], command) == -1){
      		exit(EXIT_FAILURE);
    	}
  	} else {
    	int status;
    	wait(&status);
  	}
}


int main(){
  	if(fopen("file.txt", "r") == NULL){
    	char *url = "https://drive.google.com/u/0/" "uc?id=1HWx1BGi1rEZjoEjzhAuKMAcdI27wQ41C&export=download";
    	cmd((char *[]){"wget", url, "-O", "file.txt", "-q", NULL});
  	}

  	int fd1[2], fd2[2];
  	if(pipe(fd1) < 0 || pipe(fd2) < 0){
    	fprintf(stderr, "ERROR: failed to create pipe!\n");
    	exit(1);
  	}

  	pid_t pid = fork();

  	if(pid < 0){
    	fprintf(stderr, "ERROR: failed to create fork\n");
    	exit(1);
  	}

  	if(pid == 0){
	    close(fd1[0]);
	    close(fd2[1]);
	
	    char text[1024];
	    int charFrequency[26];
	
		read(fd2[0], text, sizeof(text));
	    read(fd2[0], charFrequency, sizeof(charFrequency));
	
	    HuffmanNode *huffmanTree = createHuffmanTree(charFrequency);
	
	    char huffmanMap[26][16] = {""};
	    traverseHuffmanTree(huffmanTree, "", huffmanMap);
	
	    printf("\n----- HUFFMAN CODE TABLE -----\n");
	    
		for(int i = 0; i < 26; i++){
	    	if(strcmp(huffmanMap[i], "") != 0){
	        	printf("%c: %s\n", (char)(i + 'A'), huffmanMap[i]);
	      	}
	    }

	    char huffmanEncodedText[4096] = "";
	    for (int i = 0; text[i] != '\0'; i++){
	      	if (text[i] >= 'A' && text[i] <= 'Z'){
	        	strcat(huffmanEncodedText, huffmanMap[text[i] - 'A']);
	      	}
	    }
	
    	printf("\n----- ENCODED TEXT -----\n");
    	printf("%s\n", huffmanEncodedText);

		write(fd1[1], huffmanMap, sizeof(huffmanMap));
    	write(fd1[1], huffmanEncodedText, strlen(huffmanEncodedText) + 1);
  	} else {
    	close(fd1[1]);
    	close(fd2[0]);

    	FILE *fp = fopen("./file.txt", "r");

    	char text[1024];
	    int charFrequency[26] = {0}, charCount = 0;
	    int c, i = 0;

	    while ((c = fgetc(fp)) != EOF) {
		  	char ch = (char)c;
		    if (ch >= 'a' && ch <= 'z') {
		      	ch -= 32;
		    	charFrequency[ch - 'A']++;
		        charCount++;
		    } else if (ch >= 'A' && ch <= 'Z') {
		        charFrequency[ch - 'A']++;
		        charCount++;
		    }
		    text[i++] = ch;
	    }

	    text[i] = '\0';
	
	    printf("----- ORIGINAL TEXT -----\n");
	    printf("%s\n", text);
	
	    write(fd2[1], text, sizeof(text));
	    write(fd2[1], charFrequency, sizeof(charFrequency));
	
	    char huffmanEncodedText[4096];
	    char huffmanMap[26][16];

	    HuffmanNode *huffmanTree = createHuffmanTree(charFrequency);
	
	    read(fd1[0], huffmanMap, sizeof(huffmanMap));
	    read(fd1[0], huffmanEncodedText, sizeof(huffmanEncodedText));
	
	    printf("\n----- DECODED TEXT -----\n");
	
	    HuffmanNode *explorer = huffmanTree;
	    for (int i = 0; huffmanEncodedText[i] != '\0'; i++) {
	      	if(huffmanEncodedText[i] == '0' && explorer->left != NULL) {
	        	explorer = explorer->left;
	      	} else if (huffmanEncodedText[i] == '1' && explorer->right != NULL) {
	        	explorer = explorer->right;
	      	}
	
	      	if (explorer->left == NULL && explorer->right == NULL) {
	        	printf("%c", explorer->character);
	        	explorer = huffmanTree; // back to root
	    	}
	    }
	    printf("\n");
	
		printf("\n----- SUMMARY -----\n");
		printf("Before compression\n");
	
		int totalUncompressed = charCount * 8;
		printf("Total size: %d bits\n\n", totalUncompressed);
	
		printf("After compression\n");
		int msgSize = 0;
	    
		for (int i = 0; i < 26; i++) {
	    	msgSize += charFrequency[i] * strlen(huffmanMap[i]);
	    }
	    printf("Message: %d bits\n", msgSize);
	
	    int tableSize = 0;
	    for (int i = 0; i < 26; i++) {
	      	if (strcmp(huffmanMap[i], "") != 0) {
	        	tableSize += 8 * strlen(huffmanMap[i]);
	      	}
	    }
	
	    printf("Table: %d bits\n", tableSize);
	
	    int totalCompressed = msgSize + tableSize;
	    printf("Total size: %d bits\n\n", msgSize + tableSize);
	
	    double reduction =
	        100 * (1.0 - ((double)totalCompressed / (double)totalUncompressed));
	    	printf("Size reduced by %.2f%%\n", reduction);
		}

  	return 0;
}